#[macro_use] extern crate mioco;
#[macro_use] extern crate log;
extern crate logger;
extern crate libalt;

use libalt::dispatcher::*;
use libalt::packet::Context;
use libalt::message::Msg;
use libalt::event::*;
use std::net::SocketAddr;
use std::str::FromStr;

fn main(){
    logger::init();

    let mut mioco = mioco::Mioco::new_configured(libalt::mioco_conf());
    mioco.start(move ||{
        let mut disp_a = Dispatcher::new(); // client <--> proxy
        let mut disp_b = Dispatcher::new(); // proxy <--> server

        disp_a.context_in = Context::Server;
        disp_a.context_out = Context::Client;
        disp_b.context_in = Context::Client;
        disp_b.context_out = Context::Server;

        // client <- rx_a -> proxy <- rx_b -> server
        let rx_a = disp_a.catchall(27280);
        let rx_b = disp_b.catchall(27270);
        let sx_a = disp_a.sender();
        let sx_b = disp_b.sender();

        mioco::spawn(move || disp_a.run());
        mioco::spawn(move || disp_b.run());

        let server = SocketAddr::from_str("127.0.0.1:27276").unwrap();
        let mut client = None;
        let mods = [ ev_mods, read_join ];

        loop{ select!(
            r:rx_a => {
                if let Ok(mut msg) = rx_a.try_recv(){
                    client = Some(msg.addr);
                    for m in &mods { msg = m(msg, Context::Client); }
                    msg.addr = server;
                    msg.port = 27270;
                    debug!("client <- server {:?}", msg);
                    sx_b.send(msg).unwrap();
                }
            },
            r:rx_b => {
                if let Ok(mut msg) = rx_b.try_recv(){
                    if let Some(addr) = client{
                        for m in &mods { msg = m(msg, Context::Server); }
                        msg.addr = addr;
                        msg.port = 27280;
                        debug!("server <- client {:?}", msg);
                        sx_a.send(msg).unwrap();
                    }
                }
            },
        );}
    }).unwrap();
}

fn read_join(msg: Msg, _: Context) -> Msg{
    use libalt::net::join_msg::JoinMsg;
    match msg.downcast::<JoinMsg>(){
        Ok(msg) => {
            info!("Join message: {:?}", msg);
            msg.upcast()
        }
        Err(msg) => msg
    }
}

fn ev_mods(msg: Msg, sender: Context) -> Msg{
    use libalt::net::game_msg::GameMsg;
    let mods = [ lvl_60 ];

    match msg.downcast::<GameMsg>(){
        Ok(mut msg) => {
            msg.data.events(|ev| for m in &mods { m(ev, sender); });
            msg.upcast()
        }
        Err(msg) => msg
    }
}

fn lvl_60(ev: &mut Event, _: Context){
    match *ev{
        Event::PlayerInfo(ref mut e) => e.level = 60,
        _ => ()
    }
}
