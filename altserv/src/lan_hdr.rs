use libalt::dispatcher::*;
use libalt::net::lan_msg::LanMsg;
use std::net::SocketAddr;

pub fn start(disp: &mut Dispatcher, port: u16, addr: SocketAddr){
    let sx = disp.sender();
    let rx = disp.receiver::<LanMsg>(port);
    info!("Starting LAN handler");
    ::mioco::spawn(move ||{
        loop{
            let req = recv::<LanMsg>(&rx).unwrap();
            let mut resp = req.reply(LanMsg::Response(Some(addr)));
            resp.port = 27276;
            send(&sx, resp).unwrap();
        }
    });
}
