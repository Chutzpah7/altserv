use libalt::dispatcher::*;
use libalt::net::join_msg::*;
use libalt::event::*;
use config::Config;
use client::*;

use std::collections::HashMap;

/// Starts a join handler.
///
/// Listens for join/disconnect messages, checks password and level requirements
/// (if any), and adds the client to the client map.
pub fn start(disp: &mut Dispatcher, port: u16, clients: ClientMap,
             conf: Config){
    let sx = disp.sender();
    let rx = disp.receiver::<JoinMsg>(port);
    let mut joining = HashMap::new();
    let mut player_id = 0;

    info!("Starting join handler");
    ::mioco::spawn(move ||{
        loop{
            use libalt::net::join_msg::JoinMsg::*;
            // Wait for a new join message
            let msg = recv::<JoinMsg>(&rx).unwrap();
            match *msg.data{
                JoinReq(ref req) => {

                    if let Some(reason) = check_join(&conf, &req){
                        let reply = JoinResp(RespData{
                            allow: false, reason: Some(reason)});
                        send(&sx, msg.reply(reply)).unwrap();
                        continue;
                    }
                    let reply = JoinResp(RespData{allow: true, reason: None});
                    send(&sx, msg.reply(reply)).unwrap();
                    let client = Client::new(msg.addr, port, req.id.clone(),
                            conf.map().resource, sx.clone(), conf.clone(),
                            player_id, clients.clone());
                    player_id += 1;
                    joining.insert(msg.addr, client);

                },
                JoinResp(_) => warn!("got join response?"),
                LoadReq(_) => {

                    if let Some(mut client) = joining.remove(&msg.addr){
                        for c in clients.lock().iter_mut(){
                            client.send_ev(
                                Event::PlayerInfo(c.1.make_join_ev()));
                        }
                        client.sync();
                        clients.send_ev(
                            Event::PlayerInfo(client.make_join_ev()));
                        info!("{} has joined from {}",client.id.nick,msg.addr);
                        clients.add(client);
                    }

                },
                LoadResp(_) => warn!("got load response?"),
                Disconnect => {
                    if let Some(client) = clients.remove(&msg.addr){
                        info!("Got disconnect request from {}",msg.addr);
                        clients.send_ev(
                            Event::PlayerInfo(client.make_leave_ev()));
                    }
                },
                KeepAlive => warn!("got keepalive, not handled"),
            }
        }
    });
}

fn check_join(conf: &Config, data: &ReqData) -> Option<String>{
    let conf = conf.read();
    if let Some(ref pass) = conf.pass{
        if data.pass != *pass {
            return Some("Incorrect password".to_string());
        }
    }
    None
}
