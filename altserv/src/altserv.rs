#[macro_use] extern crate mioco;
#[macro_use] extern crate log;
extern crate logger;
extern crate libalt;
extern crate math;

mod game_server;
mod config;
mod ping_hdr;
mod lan_hdr;
mod info_hdr;
mod join_hdr;
mod game;
mod client;
mod chat;
mod commands;

use std::collections::HashMap;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;
use mioco::sync::{Mutex,mpsc};
use game_server::{ServerMsg,Server,ServerHandle};
use config::ManagerConf;
use libalt::net::server_info::ServerInfo;
use libalt::dispatcher::Dispatcher;

#[derive(Debug,Clone)]
pub enum ManagerMsg{
    StartServer(String),
    StopServer(String),
    SetInfo(u16,ServerInfo),
    ServerMsg(String,ServerMsg),
}

pub type ServerList = Arc<Mutex<HashMap<u16, ServerInfo>>>;

struct ServerManager{
    conf: ManagerConf,
    servers: HashMap<String, ServerHandle>,
    to_self: mpsc::Sender<ManagerMsg>,
    info: ServerList,
}

impl ServerManager{
    pub fn start(){
        let conf = ManagerConf::load("altserv.cfg");
        let (sx,rx) = mpsc::channel();
        let info = Arc::new(Mutex::new(HashMap::new()));

        let mut mgr = ServerManager{
            conf: conf,
            servers: HashMap::new(),
            to_self: sx,
            info: info,
        };

        let mut mioco_conf = mioco::Config::new();
        mioco_conf.event_loop().timer_tick_ms(game::TICK_MS as u64);
        let mut mioco = mioco::Mioco::new_configured(mioco_conf);

        let _ = mioco.start(move || mgr.run(rx));
    }

    pub fn run(&mut self, rx: mpsc::Receiver<ManagerMsg>){
        info!("Starting {} servers.", self.conf.servers.len());
        let mut disp = Dispatcher::new();
        for s in &self.conf.servers{
            self.servers.insert(s.clone(),
                Server::start(s.clone(), self.to_self.clone(), &mut disp));
        }
        let addr = SocketAddr::from_str("127.0.0.1:27260").unwrap();
        lan_hdr::start(&mut disp, 27260, addr);
        info_hdr::start(&mut disp, 27276, self.info.clone());

        mioco::spawn(|| disp.run());

        loop{
            use ManagerMsg::*;
            match rx.recv().unwrap(){
                ServerMsg(id, msg) => {
                    if let Some(s) = self.servers.get(&id){
                        s.send(msg);
                    }
                },
                SetInfo(port, info) => {
                    let mut x = match self.info.lock(){
                        Ok(guard) => guard,
                        Err(poi) => poi.into_inner(),
                    };
                    x.insert(port, info);
                },
                _ => unimplemented!(),
            }

        }
    }
}

fn main() {
    logger::init();
    info!("Logger initialised");
    ServerManager::start();
}

