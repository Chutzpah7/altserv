use mioco::sync::mpsc::*;
use mioco::timer::*;
use mioco;

use libalt::dispatcher::*;
use libalt::net::game_msg::*;
use libalt::message::*;
use config::Config;
use client::*;

pub const TICK_MS: i64 = 15;

pub struct Game{
    clients: ClientMap,
    //conf:    Config,
    //sx:      Sender<Msg>,
    rx:      Receiver<Msg>,
    active:  bool,
}

impl Game{
    pub fn start(disp: &mut Dispatcher, port: u16, clients: ClientMap,
    _conf: Config){
        let _sx = disp.sender();
        let rx = disp.receiver::<GameMsg>(port);

        let game = Game{
            clients: clients,
            //conf:    conf,
            //sx:      sx,
            rx:      rx,
            active:  false,
        };

        mioco::spawn(move || game.run() );
    }

    fn run(mut self){
        use mioco::Evented;

        info!("Starting main game loop, tick: {}ms",TICK_MS);
        let mut timer = Timer::new();
        timer.set_timeout(TICK_MS);

        loop{
            unsafe{
                if self.active {
                    timer.select_add(mioco::RW::read());
                }
                self.rx.select_add(mioco::RW::read());
            }
            let was_active = self.active;
            let ret = mioco::select_wait();
            if self.active && ret.id() == timer.id(){
                self.tick();
                self.active = self.clients.lock().len() > 0;
                if self.active {
                    timer.set_timeout(TICK_MS);
                }
            }
            if ret.id() == self.rx.id(){
                if let Ok(msg) = try_recv::<GameMsg>(&self.rx) { self.recv(msg) }
                self.active = true;
                if !was_active {
                    timer.set_timeout(TICK_MS);
                }
            }
        }
    }

    fn tick(&mut self){
        for c in self.clients.lock().iter_mut(){
            c.1.tick();
        }
    }

    fn recv(&mut self, msg: Message<GameMsg>){
        let mut clients = self.clients.lock();
        if let Some(c) = clients.get_mut(&msg.addr){
            c.msg_in(msg.data);
        }
    }

}
