use client::*;

use libalt::event::*;
use libalt::net::playerno::*;

pub fn say(clients: &ClientMap, player: i32, msg: &str){
    let ev = ChatEv{
        player: player as PlayerNo,
        is_team: false,
        msg: String::from(msg),
    };
    clients.send_ev(Event::Chat(ev));
}

pub fn server_whisp(client: &mut Client, msg: &str){
    let ev = ChatEv{
        player: -1 as PlayerNo,
        is_team: false,
        msg: String::from(msg),
    };
    client.send_ev(Event::Chat(ev));
}
