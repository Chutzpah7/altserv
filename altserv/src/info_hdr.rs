use libalt::dispatcher::*;
use libalt::net::info_msg::InfoMsg;
use ServerList;

pub fn start(disp: &mut Dispatcher, port: u16, servers: ServerList){
    let sx = disp.sender();
    let rx = disp.receiver::<InfoMsg>(port);
    info!("Starting info handler");
    ::mioco::spawn(move ||{
        loop{
            let req = recv::<InfoMsg>(&rx).unwrap();
            let srv = match servers.lock(){
                Ok(guard) => guard,
                Err(poi) => poi.into_inner(),
            };
            for (port,s) in &*srv{
                let mut resp = req.reply(InfoMsg::Response(s.clone()));
                resp.port = *port;
                send(&sx, resp).unwrap();
            }
        }
    });
}
