use mioco::sync::mpsc;
use config::{Config,load_config,MapSpec};
use libalt::dispatcher::Dispatcher;
use libalt::net::resource::*;
use client::*;
use ManagerMsg;

use std::net::SocketAddr;
use std::str::FromStr;

#[derive(Debug,Clone)]
pub enum ServerMsg{ }

// Used by server manager to send commands to the server
pub struct ServerHandle{
    _id:  String,
    snd: mpsc::Sender<ServerMsg>,
}

// The internal state of the game server
pub struct Server{
    _id:      String,
    conf:    Config,
    manager: mpsc::Sender<ManagerMsg>,
    clients: ClientMap,
}

impl ServerHandle{
    // Send a ServerMsg to the server
    pub fn send(&self, m: ServerMsg){
        self.snd.send(m).unwrap_or_else(|e|
            warn!("Failed to send message to a server! {}",e));
    }
}

impl Server{

    pub fn start(id: String, snd: mpsc::Sender<ManagerMsg>,
                 disp: &mut Dispatcher) -> ServerHandle{
        let server = Server{
            conf: load_config(&id),
            _id: id.clone(),
            manager: snd,
            clients: ClientMap::default(),
        };
        let snd = server.run(disp);
        ServerHandle{ _id: id, snd: snd }
    }

    pub fn run(self, disp: &mut Dispatcher) -> mpsc::Sender<ServerMsg>{
        let (sx,rx) = mpsc::channel();

        let addr = SocketAddr::from_str("127.0.0.1:27276").unwrap();
        self.manager.send( ManagerMsg::SetInfo(self.conf.port(),
            self.conf.read().server_info(&addr)))
            .unwrap_or_else(
                |e| info!("Failed to connect to server manager: {}",e));

        self.conf.write().map = MapSpec{
            resource: Resource{
                path: "ball_football.altx".to_owned(),
                size: 432838,
                crc32: 2845754500361224192},
            width: 3000,
            height: 1400,
        };

        let port = self.conf.port();
        ::ping_hdr::start_srv(disp, port);
        ::ping_hdr::start(disp, port);
        ::join_hdr::start(disp, port, self.clients.clone(), self.conf.clone());
        ::game::Game::start(disp, port, self.clients.clone(),self.conf.clone());

        // Server loop, listen for commands from server manager.
        ::mioco::spawn(move ||{
            loop{
                match rx.recv().unwrap(){

                }
            }
        });
        sx
    }
}
