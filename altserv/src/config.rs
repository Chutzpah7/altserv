extern crate config;

use libalt::net::resource::*;
use libalt::net::server_conf::*;
use libalt::net::server_info::*;
use libalt::net::version;

use std::path::Path;
use std::net::SocketAddr;
use std::sync::{Arc,RwLock,RwLockReadGuard,RwLockWriteGuard};
use std::default::Default;

#[derive(Default,Debug,Clone)]
pub struct MapSpec{
    pub resource: Resource,
    pub width: u32,
    pub height: u32,
}

#[derive(Debug,Clone)]
pub struct ManagerConf{
    pub servers: Vec<String>,
}

#[derive(Debug,Clone)]
pub struct Config{ c: Arc<RwLock<Conf>> }

impl Config{
    pub fn new(c: Arc<RwLock<Conf>>) -> Config{ Config{ c: c } }
    pub fn read(&self) -> RwLockReadGuard<Conf>{
        match self.c.read(){
            Ok(guard) => guard,
            Err(poisoned) => poisoned.into_inner(),
        }
    }
    pub fn write(&self) -> RwLockWriteGuard<Conf>{
        match self.c.write(){
            Ok(guard) => guard,
            Err(poisoned) => poisoned.into_inner(),
        }
    }
    pub fn port(&self) -> u16{ self.read().port }
    pub fn map(&self) -> MapSpec{ self.read().map.clone() }
}

pub fn load_config(id: &str) -> Config{
    let path = format!("servers/{}.cfg",id);
    let conf = Conf::default();
    let conf = match conf.load(&path){ Ok(x) | Err(x) => x };
    Config::new(Arc::new(RwLock::new(conf)))
}

#[derive(Debug,Clone)]
pub struct Conf{
    pub name:      String,
    pub port:      u16,
    pub pass:      Option<String>,
    pub min_level: i32,
    pub max_level: i32,
    pub max_players: u16,
    pub map:       MapSpec,
}

impl Default for Conf{
    fn default() -> Conf{
        Conf{
            name:      "AltServ Server".to_owned(),
            port:      27276,
            pass:      None,
            min_level: 1,
            max_level: 60,
            max_players: 12,
            map:       MapSpec::default(),
        }
    }
}

impl Conf{
    pub fn load(mut self, path: &str) -> Result<Conf,Conf>{
        let res = config::reader::from_file(Path::new(path));
        match res{
            Err(e) => {
                error!("Failed to read config file {}: {}",path,e);
                Err(self)
            },
            Ok(c) =>{
                self.name = c.lookup_str_or("server_name",&self.name).to_owned();
                self.port = c.lookup_integer32_or("port",self.port as i32) as u16;
                if let Some(p) = c.lookup_str("password"){
                    self.pass = Some(p.to_owned());}
                self.min_level = c.lookup_integer32_or(
                    "min_level",self.min_level);
                self.max_level = c.lookup_integer32_or(
                    "max_level",self.max_level);
                self.max_players = c.lookup_integer32_or(
                    "max_players",self.max_players as i32) as u16;

                Ok(self)
            }
        }
    }
    pub fn server_conf(&self) -> ServerConf{
        let mut ret = ServerConf::new();
        ret.max_players = self.max_players;
        ret
    }
    pub fn server_info(&self, addr: &SocketAddr) -> ServerInfo{
        ServerInfo{
            id:            1,
            addr:          Some(*addr),
            map_name:      Some(self.map.resource.path.clone()),
            max_players:   self.max_players,
            num_players:   0,
            server_name:   Some(self.name.clone()),
            pass_req:      self.pass.is_some(),
            hardcore:      true,
            min_level:     self.min_level,
            max_level:     self.max_level,
            disallow_demo: false,
            version:       version::VERSION,
        }
    }
}

impl Default for ManagerConf{
    fn default() -> ManagerConf{
        ManagerConf{
            servers: Vec::new(),
        }
    }
}

impl ManagerConf{
    pub fn load(path: &str) -> ManagerConf{
        let mut conf = ManagerConf::default();
        let res = config::reader::from_file(Path::new(path));
        match res{
            Err(_) => {
                panic!("Failed to read main config file altserv.cfg");
            },
            Ok(c) =>{
                let mut i = 0;
                debug!("config: {:?}",c);
                conf.servers.clear();
                while let Some(s) = c.lookup_str(&format!("servers.[{}]",i)){
                    conf.servers.push(s.to_owned());
                    i += 1;
                }
                if i == 0{ error!("No servers specified in config");}
                conf
            },
        }
    }
}

