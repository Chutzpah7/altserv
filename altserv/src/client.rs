use std::net::SocketAddr;
use std::collections::{HashMap,VecDeque};
use std::sync::{Arc,MutexGuard};
use std::default::Default;

use mioco;
use mioco::sync::Mutex;
use mioco::sync::mpsc::*;

use libalt::net::client_id::*;
use libalt::guarantee::Level as GLevel;
use libalt::net::resource::*;
use libalt::dispatcher::send;
use libalt::message::*;
use libalt::event_mgr::*;
use libalt::event::*;
use libalt::net::game_msg::*;
use libalt::net::plane_setup::*;
use libalt::net::team::*;
use math::Vec2f;

use config::Config;
use chat;
use commands;

pub struct Client{
    pub id:        ClientID,
    pub addr:      SocketAddr,
    pub player_no: i32,
    pub events:    VecDeque<Event>,
    server_port:   u16,
    sx:            Sender<Message<MsgData>>,
    config:        Config,
    ev_mgr:        EventMgr,
    clients:       ClientMap,
    view_pos:      Vec2f,
}
#[derive(Clone)]
pub struct ClientMap{ x: Arc<Mutex<HashMap<SocketAddr, Client>>> }

impl Client{
    pub fn new(addr: SocketAddr, port: u16, id: ClientID, map: Resource,
    sx: Sender<Msg>, conf: Config, player_no: i32, clients: ClientMap) -> Client{
        let mut client = Client{
            id:          id,
            addr:        addr,
            player_no:   player_no,
            events:      VecDeque::new(),
            server_port: port,
            sx:          sx.clone(),
            config:      conf,
            ev_mgr:      EventMgr::new(sx,addr,port),
            clients:     clients,
            view_pos:    Vec2f::new(0f32,0f32),
        };
        client.send_map(map);
        client
    }

    pub fn send_map(&mut self, map: Resource){
        use libalt::net::join_msg::*;

        let msg = JoinMsg::LoadResp(LoadRespData{
                      counter:  self.ev_mgr.inc_counter(),
                      resource: map,
                      conf:     self.config.read().server_conf(),
                  });

        self.send_msg(msg, GLevel::Order);
    }

    pub fn send_ev(&mut self, ev: Event){
        self.ev_mgr.send(ev);
    }

    pub fn sync(&mut self){

        self.ev_mgr.send(Event::TeamSet(
                TeamEv{ player: self.player_no, team: Team::Spec }));
        let join_ev = self.make_join_ev();
        self.ev_mgr.send(Event::PlayerInfo(join_ev));

        // TODO: send all entities

        chat::server_whisp(self, "Welcome to altserv!");
    }

    pub fn tick(&mut self){
        self.ev_mgr.tick();
    }

    pub fn msg_in(&mut self, msg: Box<GameMsg>){
        self.ev_mgr.msg_in(msg);
        while let Some(ev) = self.ev_mgr.next(){
            match ev{
                Event::Chat(x) => chat::say(&self.clients, self.player_no, &x.msg),
                Event::View(x) => self.view_pos = x.pos,
                Event::Command(x) => commands::run(self, x.command),
                x => {
                    chat::server_whisp(self, &format!("unknown ev: {:?}",x));
                    info!("event not handled: {:?}",x);
                    self.events.push_back(x);
                }
            }
        }
    }

    pub fn make_join_ev(&self) -> PlayerInfoEv{
        self.player_info_ev(true, None)
    }
    pub fn make_leave_ev(&self) -> PlayerInfoEv{
        self.player_info_ev(false, Some(String::from("reasons >_>")))
    }

    pub fn player_info_ev(&self, join: bool, reason: Option<String>)
    -> PlayerInfoEv{
        PlayerInfoEv{
            join:   join,
            id:     self.player_no,
            reason: reason,
            nick:   self.id.nick.clone(),
            uuid:   self.id.vapor.clone(),
            unk1:   0,
            setup:  PlaneSetup::default(),
            team:   Team::Spec,
            level:  self.id.level.level as u64,
            ace:    self.id.level.ace as u64,
        }
    }

    fn send_msg<T>(&self, msg: T, g: GLevel) where T: MsgData{
        let msg = Message::new(self.addr, self.server_port, g, msg);
        send(&self.sx, msg).unwrap();
    }
}

impl Default for ClientMap{
    fn default() -> ClientMap{
        ClientMap{ x: Arc::new(Mutex::new(HashMap::new())) }
    }
}

impl ClientMap{
    pub fn lock(&self) -> MutexGuard<HashMap<SocketAddr, Client>>{
        match self.x.lock(){
            Ok(guard) => guard,
            Err(poison) => poison.into_inner()
        }
    }
    pub fn add(&self, client: Client){
        self.lock().insert(client.addr,client);
    }
    pub fn remove(&self, addr: &SocketAddr) -> Option<Client>{
        self.lock().remove(addr)
    }
    pub fn send_ev(&self, ev: Event){
        let clients = self.clone();
        mioco::spawn(move || {
            for c in clients.lock().iter_mut(){
                c.1.send_ev(ev.clone());
            }
        });
    }
}
