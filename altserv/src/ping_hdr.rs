use libalt::dispatcher::*;
use libalt::net::ping_msg::*;

pub fn start(disp: &mut Dispatcher, port: u16){
    let sx = disp.sender();
    let rx = disp.receiver::<PingMsg>(port);
    ::mioco::spawn(move ||{
        loop{
            let ping = recv::<PingMsg>(&rx).unwrap();
            let reply = PingMsg{ id: ping.data.id, respond: false };
            send(&sx, ping.reply(reply)).unwrap();
        }
    });
}

pub fn start_srv(disp: &mut Dispatcher, port: u16){
    let sx = disp.sender();
    let rx = disp.receiver::<ServerPingMsg>(port);
    ::mioco::spawn(move ||{
        loop{
            let ping = recv::<ServerPingMsg>(&rx).unwrap();
            let reply = ServerPingMsg{ id: ping.data.id, respond: false };
            send(&sx, ping.reply(reply)).unwrap();
        }
    });
}
