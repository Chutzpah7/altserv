#[macro_use] extern crate log;
extern crate loggerv;
extern crate libalt;

use libalt::socket::*;
use libalt::packet::Context;

use std::io::Read;
use std::fs::File;
use std::net::SocketAddr;
use std::str::FromStr;

fn main(){
    loggerv::init_with_verbosity(1).expect("Failed to start logger");
    let addr = SocketAddr::from_str("0.0.0.0:0").unwrap();
    let port = 0;

    let mut file = File::open("msg.bin").expect("failed to open msg.bin");
    let mut data = Vec::new();
    file.read_to_end(&mut data).expect("failed to read file data");
    let msg = make_game_msg(data.as_slice(), addr, port, Context::Client);

    info!("{:#?}",msg);
}
