extern crate log;
extern crate env_logger;
extern crate colored;

use std::env;
use colored::*;
use log::{LogRecord,LogLevelFilter};
use env_logger::LogBuilder;

/// Start envlogger with a custom format, using the RUST_LOG environmental
/// variable to set the log level.
pub fn init(){
    let format = |record: &LogRecord| {
        use log::LogLevel::*;
        let location = record.location().module_path();
        let location = match record.level(){
            Error => location.red(),
            Warn => location.yellow(),
            Info => location.green().dimmed(),
            Debug => location.cyan().dimmed(),
            Trace => location.blue().dimmed(),
        };
        format!("{:15} {}", location, record.args())
    };
    let mut builder = LogBuilder::new();
    builder.format(format).filter(None, LogLevelFilter::Info);
    if env::var("RUST_LOG").is_ok() {
        builder.parse(&env::var("RUST_LOG").unwrap());
    }
    builder.init().unwrap();
}
