/// Creates abstarctions over sockets which convert data to and from
/// the Message format.
///
/// Also contains functions for converting raw data to Messages for
/// debugging purposes.

use message::*;
use packet::*;
use state::*;
use net::netable::Netable;
use dispatcher::DispMsg;
use mioco;
use mioco::sync::mpsc::*;
use mioco::udp::UdpSocket as MUdpSocket;
use mio::udp::UdpSocket;
use mio::{IpAddr,Ipv4Addr};
use std::net::{SocketAddr,ToSocketAddrs};
use std::sync::{Arc,Mutex,MutexGuard};

#[derive(Debug,Clone)]
pub struct SocketMsg{
    x: Arc<Mutex<Msg>>,
}

impl SocketMsg{
    pub fn new(msg: Msg) -> SocketMsg{
        SocketMsg{ x: Arc::new(Mutex::new(msg)) }
    }
    pub fn get(&self) -> MutexGuard<Msg>{
        match self.x.lock(){
            Ok(guard) => guard,
            Err(poisoned) => poisoned.into_inner(),
        }
    }
}

/// Opens a UDP socket on the specified port and converts between
/// messagesg and datagrams.
///
/// The first argument is the sender over which incoming messages will be
/// sent. The function returns a sender to send outgoing messages over
/// the socket.
pub fn game(snd: Sender<DispMsg>, port: u16, multi: bool, c_in: Context,
            c_out: Context, shared: SharedState) -> Sender<SocketMsg>{
    info!("Creating game socket on port {}",port);

    let mut socket = new_game_socket(port, multi);
    let (sx,rx) = channel();
    mioco::spawn(move ||{
        let mut buffer = [0u8;2048];
        loop{ select!(
            r:socket => {
                if let Ok(Some((len,addr))) = socket.try_recv(&mut buffer){
                    let data = &buffer[0..len];
                    let msg = make_game_msg(data, addr, port, c_in, Some(shared.clone()));
                    match msg{
                        Ok(msg) => snd.send(DispMsg::In(msg))
                                    .expect("Failed to send game message to dispatcher!"),
                        Err(e) => error!("failed to parse message: {:?}",e)
                    }
                }
            },
            r:rx => {
                if let Ok(msg) = rx.try_recv(){
                    let (len, addr) = msg_to_data(msg, &mut buffer, c_out, Some(shared.clone()));
                    socket.send(&buffer[0..len],&addr)
                        .expect("Failed to send message over socket!");
                }
            },);
        }
    });
    sx
}

/// Create a Message from raw data.
pub fn make_game_msg(buf: &[u8], addr: SocketAddr, port: u16, c: Context,
                     shared: Option<SharedState>) -> Res<Msg>{
    use guarantee::{Guarantee,Level,Data};
    let mut p = BitStreamR::new(buf.to_owned());
    p.context = c;
    p.shared = shared;

    let g_level = try!(Level::read(&mut p));
    let header = try!(p.read_u8());
    let g_data = match g_level{
        Level::Delivery | Level::Order => { try!(Data::read(&mut p)) }
        _ => { Data::None } };
    let guarantee = Guarantee{ level: g_level, data: g_data };

    let data = match guarantee.data{
        Data::Data{..} | Data::None{..} | Data::Open{..} =>
            try!(read_msg_data(&mut p, header)),
        _ => Box::new(EmptyData)
    };

    Ok(Message{ addr: addr, port: port, guarantee: guarantee, data: data })
}

/// Write a message to the given buffer. Returns the number of bytes written,
/// and the SocketAddr of the message.
pub fn msg_to_data(msg: SocketMsg, buf: &mut [u8], c: Context,
                   shared: Option<SharedState>) -> (usize, SocketAddr){
    let msg = msg.get();
    let mut p = BitStreamW::new();
    p.context = c;
    p.shared = shared;

    msg.guarantee.level.write(&mut p);
    p.write_u8(msg.data.header());
    msg.guarantee.data.write(&mut p);
    msg.data.write(&mut p);
    let d = p.data();
    buf[0..d.len()].clone_from_slice(d.as_slice());
    (d.len(), msg.addr)
}

fn new_game_socket(port: u16, multi: bool) -> MUdpSocket{
    let addr = ("0.0.0.0",port)
        .to_socket_addrs().unwrap().next().unwrap();
    let socket = UdpSocket::bound(&addr)
        .expect("Failed to bind socket!");
    if multi{
        socket.join_multicast(&IpAddr::V4(Ipv4Addr::new(227,27,27,4)))
            .expect("Failed to join multicast");
    }

    MUdpSocket::new(socket)
}


fn read_msg_data(p: &mut BitStreamR, header: u8) -> Res<Box<MsgData>>{
    use message::EmptyData;
    use net::info_msg::*;
    use net::ping_msg::*;
    use net::join_msg::*;
    use net::game_msg::*;
    use net::dl_msg::*;
    use net::lan_msg::*;
    match header{
        0  => Ok(Box::new(EmptyData)),
        7  => box_msg(InfoMsg::read(p)),
        8  => box_msg(ServerPingMsg::read(p)),
        70 => box_msg(JoinMsg::read(p)),
        71 => box_msg(GameMsg::read(p)),
        72 => box_msg(DlMsg::read(p)),
        73 => box_msg(PingMsg::read(p)),
        74 => box_msg(LanMsg::read(p)),
        _ =>  Err(UnkHeader(header))
    }
}
fn box_msg<T: MsgData>(msg: Res<T>) -> Res<Box<MsgData>> {
    msg.map(|x| Box::new(x) as Box<MsgData>)
}

