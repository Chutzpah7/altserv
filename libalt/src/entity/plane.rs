use entity::{read_map_pos,write_map_pos};
use entity::powerup::Powerup;
use entity::Entity;
use net::netable::*;
use net::playerno::*;
use net::team::*;
use net::plane_setup::*;
use packet::*;
use math::Vec2f;

#[derive(Debug,Clone)]
pub enum Plane{
    Loop(Loop),
    Bomber(Bomber),
    Whale(Whale),
    Bip(Bip),
    Randa(Randa),
}

pub type Loop = PlaneBase;
pub type Bomber = PlaneBase;
pub type Whale = PlaneBase;
pub type Bip = PlaneBase;
pub type Randa = PlaneBase;

#[derive(Debug,Clone)]
pub struct PlaneBase{
    player:  PlayerNo,
    team:    Team,
    setup:   PlaneSetup,
    pos:     Vec2f,
    angle:   f32,
    powerup: Option<Powerup>,
    unk1:    bool,
    unk2:    u16,
    unk3:    u16,
    unk4:    u8,
}

impl Netable for PlaneBase{
    fn read(p: &mut BitStreamR) -> Res<PlaneBase>{
        Ok(PlaneBase{
            player: try!(PlayerNo::read(p)),
            team:  try!(Team::read(p)),
            setup: try!(PlaneSetup::read(p)),
            pos:   try!(read_map_pos(p, 50., 1.)),
            angle: try!(p.read_range(-180,180)) as f32,
            powerup:
                if try!(p.read_bool()){
                    Some(if let Entity::Powerup(p) = try!(Entity::read(p)) { p }
                        else { panic!("player's powerup isn't a powerup")})
                } else {None},
            unk1: try!(p.read_bool()),
            unk2: try!(p.read(16)) as u16,
            unk3: try!(p.read(16)) as u16,
            unk4: try!(p.read_range(0,90)) as u8,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.player.write(p);
        self.team.write(p);
        self.setup.write(p);
        write_map_pos(p, 50., 1., self.pos);
        p.write_range(-180,180,self.angle as i32);
        match self.powerup{
            Some(ref pup) => {
                p.write_bool(true);
                (Entity::Powerup(pup.clone())).write(p);
            }
            None => p.write_bool(false),
        };
        p.write_bool(self.unk1);
        p.write_u16(self.unk2);
        p.write_u16(self.unk3);
        p.write_range(0, 90, self.unk4 as i32);
    }
}

