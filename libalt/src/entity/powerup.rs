use net::netable::*;
use packet::*;
use net::team::*;
use math::Vec2f;
use net::net_id::*;
use net::playerno::*;
use entity::{read_map_pos,write_map_pos};

#[derive(Debug,Clone)]
pub enum Powerup{
    Health(HealthP),
    Shield(ShieldP),
    Wall(WallP),
    Missile(MissileP),
    Ball(BallP),
    Charge(ChargeP),
}

pub type ShieldP = PowerupBase;
pub type WallP = PowerupBase;
pub type MissileP = PowerupBase;
pub type ChargeP = PowerupBase;

#[derive(Debug,Clone)]
pub struct PowerupBase{
    unk1: bool,
    pos: Vec2f,
    vel: Vec2f,
    team: Team,
    c: Option<f32>,
    spawner: Option<NetID>,
    player: Option<PlayerNo>,
    d: Option<i32>,
}

impl Netable for PowerupBase{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let a = try!(p.read_bool());
        let scale = if a {64f32} else {1f32};
        let pos = try!(read_map_pos(p, 300., scale));
        let vel_x = try!(p.read_srange(-50., 50., 10.*scale));
        let vel_y = try!(p.read_srange(-50., 50., 10.*scale));

        let team = try!(Team::read(p));
        let c = if !try!(p.read_bool()) {
                Some(try!(p.read_srange(0.,13.*30.,1.)))
            } else { None };

        //let spawner = Option::<NetID>::read(p);
        let spawner = if try!(p.read_bool()){
                Some(try!(NetID::read(p)))
            } else { None };

        let (player,d) = if try!(p.read_bool()){
                (Some(try!(PlayerNo::read(p))), Some(try!(p.read_range(0,300))))
            } else { (None,None) };

        Ok(PowerupBase{
            unk1:    a,
            pos:     pos,
            vel:     Vec2f::new(vel_x,vel_y),
            team:    team,
            c:       c,
            spawner: spawner,
            player:  player,
            d:       d,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_bool(self.unk1);
        let scale = if self.unk1 {64f32} else {1f32};
        write_map_pos(p, 300., scale, self.pos);
        p.write_srange(-50., 50., 10.*scale, self.vel.x);
        p.write_srange(-50., 50., 10.*scale, self.vel.y);

        self.team.write(p);
        if let Some(c) = self.c{
            p.write_bool(true);
            p.write_srange(0., 13.*30., 1., c);
        } else { p.write_bool(false); }

        self.spawner.write(p);

        if let Some(pl) = self.player{
            p.write_bool(true);
            pl.write(p);
            p.write_range(0,300,self.d.unwrap());
        } else { p.write_bool(false); }
    }
}

#[derive(Debug,Clone)]
pub struct HealthP{
    pub powerup: PowerupBase,
    pub value: u8,
    pub b: bool,
}

impl Netable for HealthP{
    fn read(p: &mut BitStreamR) -> Res<HealthP>{
        Ok(HealthP{
            powerup: try!(PowerupBase::read(p)),
            value: try!(p.read_range(0,100)) as u8,
            b: try!(p.read_bool()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.powerup.write(p);
        p.write_range(0,100, self.value as i32);
        p.write_bool(self.b);
    }
}

#[derive(Debug,Clone)]
pub struct BallP{
    pub powerup: PowerupBase,
    pub carrier: Option<(Team,PlayerNo)>,
    pub b: bool,
    pub last_carriers: [PlayerNo; 3],
}

impl Netable for BallP{
    fn read(p: &mut BitStreamR) -> Res<BallP>{
        Ok(BallP{
            powerup: try!(PowerupBase::read(p)),
            carrier: if try!(p.read_bool()){
                        Some((try!(Team::read(p)),try!(PlayerNo::read(p))))
                    } else { None },
            b: try!(p.read_bool()),
            last_carriers: [try!(PlayerNo::read(p)), try!(PlayerNo::read(p)),
                            try!(PlayerNo::read(p))],
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.powerup.write(p);
        if let Some((team,player)) = self.carrier{
            team.write(p);
            player.write(p);
        } else { p.write_bool(false); }
        p.write_bool(self.b);
        for c in &self.last_carriers{
            c.write(p);
        }
    }
}
