use net::netable::*;
use packet::*;
use math::Vec2f;

pub mod powerup;
pub mod plane;

use self::powerup::*;
use self::plane::*;

#[derive(Debug,Clone)]
pub enum Entity{
    Powerup(Powerup),
    Plane(Plane),
    Other,
}

impl Netable for Entity{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let header = try!(p.read(6));
        debug!("reading an entity: header: {}, packet pos: {}",header,p.pos());
        use self::powerup::Powerup::*;
        Ok(match header{
            6  => Entity::Plane(Plane::Bomber(try!(Bomber::read(p)))),
            12 => Entity::Plane(Plane::Whale(try!(Whale::read(p)))),
            16 => Entity::Plane(Plane::Loop(try!(Loop::read(p)))),
            30 => Entity::Powerup(Health(try!(HealthP::read(p)))),
            31 => Entity::Powerup(Wall(try!(WallP::read(p)))),
            32 => Entity::Powerup(Missile(try!(MissileP::read(p)))),
            34 => Entity::Powerup(Shield(try!(ShieldP::read(p)))),
            37 => Entity::Powerup(Charge(try!(ChargeP::read(p)))),
            39 => Entity::Powerup(Ball(try!(BallP::read(p)))),
            x => {
                error!("unknown entity header: {}",x);
                return Err(Unimplemented);
            }
        })
    }
    fn write(&self, p: &mut BitStreamW){
        use self::Entity::*;
        use self::powerup::Powerup::*;
        use self::plane::Plane::*;
        match *self{
            Plane(ref y) =>
                match *y {
                    Loop(ref x) => { p.write(6,16); x.write(p); }
                    Bomber(ref x) => { p.write(6,6); x.write(p); }
                    Whale(ref x) => { p.write(6,12); x.write(p); }
                    Bip(ref x) => { p.write(6,10); x.write(p); }
                    Randa(ref x) => { p.write(6,21); x.write(p); }
                },
            Powerup(ref y) =>
                match *y {
                    Health(ref x) => { p.write(6,30); x.write(p); }
                    Wall(ref x) => { p.write(6,31); x.write(p); }
                    Missile(ref x) => { p.write(6,32); x.write(p); }
                    Shield(ref x) => { p.write(6,34); x.write(p); }
                    Charge(ref x) => { p.write(6,37); x.write(p); }
                    Ball(ref x) => { p.write(6,39); x.write(p); }
                },
            ref x => {
                error!("unimplemented entity write: {:?}",x);
                unimplemented!();
            },
        }
    }
}

pub fn get_map_size(shared: &Option<::state::SharedState>) -> Vec2f{
    match *shared{
        Some(ref state) => {
            let lock = state.read();
            lock.map_size()
        }
        None => {
            warn!("packet has no shared state, assuming a medium map size");
            Vec2f::new(3000.,1500.)
        }
    }
}

pub fn read_map_pos(p: &mut BitStreamR, pad: f32, scale: f32) -> Res<Vec2f>{
    let map_size = get_map_size(&p.shared);
    let pos_x = try!(p.read_srange(-pad, map_size.x+pad, scale));
    let pos_y = try!(p.read_srange(-pad, map_size.y+pad, scale));
    Ok(Vec2f::new(pos_x,pos_y))
}

pub fn write_map_pos(p: &mut BitStreamW, pad: f32, scale: f32, vec: Vec2f){
    let map_size = get_map_size(&p.shared);
    p.write_srange(-pad, map_size.x+pad, scale, vec.x);
    p.write_srange(-pad, map_size.y+pad, scale, vec.y);
}
