//! Libalt's representation of an Altitude packet.
//!
//! A message contains a remote address, a local port, delivery guarantee
//! information, and the message data itsself. The message data can be
//! anything which implements the MsgData trait, which has functions for
//! writing the data to a packet.
//!
//! MsgData uses [MOPA](github.com/chris-morgan/mopa) to allow casting between
//! it and types which implement it. This is used to create an interface
//! where you can request a receiver for messages of a certain type, it can
//! also be used to write custom data to a packet by implementing MsgData.

use std::net::{SocketAddr,ToSocketAddrs};
use std::fmt::Debug;
use mopa::Any;
use guarantee::{Guarantee,Level,Data};
use packet::BitStreamW;

pub type Msg = Message<MsgData>;

pub trait MsgData: Any +Send +Debug{
    fn header(&self) -> u8;
    fn write(&self, p: &mut BitStreamW);
}
mopafy!(MsgData);

#[derive(Clone, Debug)]
pub struct EmptyData;
impl MsgData for EmptyData{
    fn header(&self) -> u8 { 0 }
    fn write(&self, _p: &mut BitStreamW){ }
}

#[derive(Clone, Debug)]
pub struct Message<T: MsgData + ?Sized>{
    /// The external address of the sender/recipient
    pub addr: SocketAddr,
    /// The *local* port
    pub port: u16,
    /// Delivery guarantee information, mostly used internaly
    pub guarantee: Guarantee,
    pub data: Box<T>,
}

impl<T: MsgData + ?Sized> Message<T>{
    /// Create a message in reply to another.
    ///
    /// Copies the address, local port, and guarantee level to a new message,
    /// adding the data passed.
    pub fn reply<A: MsgData>(&self, data: A) -> Message<A>{
        Message{
            addr: self.addr.clone(),
            port: self.port,
            guarantee: Guarantee{
                level: self.guarantee.level, data: Data::None },
            data: Box::new(data)}
    }
}

impl<T: MsgData> Message<T>{
    /// Cast the message to a generic Msg.
    pub fn upcast(self) -> Msg{
        Message{
            addr: self.addr, guarantee: self.guarantee, port: self.port,
            data: self.data as Box<MsgData>
        }
    }

    /// Create a new message.
    pub fn new<A>(addr: A, port: u16,  g: Level, data: T)
    -> Message<T> where A: ToSocketAddrs{
        let g = Guarantee{ level: g, data: Data::None };
        let addr = addr.to_socket_addrs().ok()
            .expect("Failed to parse socket address!").next()
            .expect("No socket address given!");
        Message{ addr: addr, port: port, guarantee: g, data: Box::new(data)}
    }
}

impl Msg{
    /// Attemts to cast a generic Msg to a specific type
    /// of message
    pub fn downcast<T>(self) -> Result<Message<T>, Msg>
    where T: MsgData{
        match self.data.downcast::<T>(){
            Ok(x) => Ok(Message{
                data: x,
                addr: self.addr,
                port: self.port,
                guarantee: self.guarantee}),
            Err(x) => Err(Message{
                data: x as Box<MsgData>, .. self})
        }
    }
}
