use std::cmp;
use std::mem;
use state::SharedState;
use std::io::{self, Seek, SeekFrom};

pub fn bits(v: u32)->usize{ 32 - (v-1).leading_zeros() as usize }

/// The context of a message being read, server or client.
///
/// Some things in the altitude message format are different depening
/// on if it's a client or server sending/receiving the message, this
/// enum is used in packets to give this information to the read/write
/// functions of [Netables](../trait.Netable.html)
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
pub enum Context{ Client, Server }

#[derive(Debug)]
pub enum Error{
    IOError(io::Error),
    NotEnoughData,
    Unimplemented,
    UnkHeader(u8),
}
pub use self::Error::*;

pub type Res<T> = Result<T,Error>;

pub struct BitStream<S>{
    stream: S,
    byte: usize,
    b_off: usize,
    le: bool,
    buf_valid: bool,
}

impl BitStream{
    pub fn new(stream: S) -> BitStream<S>{
        BitStreamR{
            stream:    stream,
            byte:      0,
            b_off:     8,
            le:        false,
            buf_valid: false
        }
    }
}

impl<S: Seek> Seek for BitStream<S>{
    /// Move the the given poisition in bits
    pub fn seek(&mut self, pos: SeekFrom) -> Res<u64> {
        let new = match pos {
            Start(x)   => x,
            End(x)     => 8 * try!(self.stream.seek(SeekFrom::End(0))) + x,
            Current(x) => 8 * try!(self.stream.seek(SeekFrom::Current(0)))
                            + self.b_off + x,
        };
        try!(self.stream.seek(SeekFrom::Start(new / 8)));
        self.b_off = 8 - (new % 8);
        self.buf_valid = false;
        Ok(new)
    }
}

impl<S: Seek> BitStream<S>{
    /// Get the length of the data in bytes
    pub fn len(&mut self) -> Res<u64>{
        let current =
    }

    /// The position in bits of the reader
    pub fn pos(&mut self) -> usize{
        (self.byte+1) * 8 - self.b_off
    }

    /// Ignore `x` bits from the current position
    pub fn ignore(&mut self, x: usize) -> Res<()>{
        let pos = self.pos() + x;
        try!(self.seek(pos));
        Ok(())
    }
}

impl<S: Read> BitStream<S>{

    pub fn read(&mut self, size: usize) -> Res<u64>{
        if size > 64 {
            panic!("BitStreamR::read size must be <= 64 bits");
        }
        if self.byte >= self.len(){ return Err(NotEnoughData); }
        let mut ret = 0u64;
        let mut left = size;
        while left > 0 {
            if self.b_off == 0{
                self.b_off = 8;
                self.byte += 1;
                if self.byte >= self.len(){ return Err(NotEnoughData); }
            }
            let x = cmp::min(left, self.b_off);
            ret |= (((self.data[self.byte] >> (8-self.b_off)) as u64)
                        & !((-1i64 as u64) << x))
                   << (size - left);
            self.b_off -= x;
            left -= x;
        }
        Ok(ret)
    }

    pub fn read_bool(&mut self)->Res<bool>{ Ok(try!(self.read(1)) == 1) }
    pub fn read_u8 (&mut self)->Res<u8 >{ Ok(try!(self.read(8 )) as u8 ) }
    pub fn read_u16(&mut self)->Res<u16>{ Ok(try!(self.read(16)) as u16) }
    pub fn read_u32(&mut self)->Res<u32>{ Ok(try!(self.read(32)) as u32) }
    pub fn read_u64(&mut self)->Res<u64>{ Ok(try!(self.read(64)) as u64) }

    pub fn read_i8 (&mut self)->Res<i8 >{ Ok(try!(self.read(8 )) as i8 ) }
    pub fn read_i16(&mut self)->Res<i16>{ Ok(try!(self.read(16)) as i16) }
    pub fn read_i32(&mut self)->Res<i32>{ Ok(try!(self.read(32)) as i32) }
    pub fn read_i64(&mut self)->Res<i64>{
        let i = try!(self.read(64)) as u64;
        Ok((i >> 32 | i << 32) as i64)
    }

    pub fn read_f64(&mut self)->Res<f64>{
        warn!("transmuting for f64, is this correct?");
        Ok(unsafe{ mem::transmute(try!(self.read_u64())) })
    }

    pub fn read_f32(&mut self)->Res<f32>{
        Ok(unsafe{ mem::transmute(try!(self.read_u32())) })
    }

    pub fn read_bytes(&mut self, n: u64) -> Res<Vec<u8>>{
        let mut data = Vec::new();
        for _ in 0..n { data.push(try!(self.read_u8())) }
        Ok(data)
    }
    pub fn read_data(&mut self, mut bits: usize) -> Res<Vec<u8>>{
        let mut data = Vec::new();
        while bits > 8{
            data.push(try!(self.read_u8()));
            bits -= 8;
        }
        if bits > 0 { data.push(try!(self.read(bits)) as u8); }
        Ok(data)
    }

    pub fn read_range(&mut self, min: i32, max: i32)->Res<i32>{
        if min >= max { panic!("read_range min >= max"); }
        let b = bits((max-min) as u32 + 1);
        let v = try!(self.read(b)) as i32 + min;
        if v > max { warn!("read_range value > max ({}->{} {})",min ,max ,v); }
        Ok(v)
    }
    pub fn read_srange(&mut self, min: f32, max: f32, scale: f32)->Res<f32>{
        let a = (min * scale).round() as i32;
        let b = (max * scale).round() as i32;
        Ok((try!(self.read_range(a,b)) as f32) / scale)
    }

    pub fn read_str(&mut self) -> Res<String>{
        let len = try!(self.read_u16());
        if len as usize > self.len(){
            error!("read_str length exceeds packet size!");
            return Err(NotEnoughData);
        }
        let mut s = String::new();
        for _i in 0..len { s.push(try!(self.read_u8()) as char); }
        Ok(s)
    }
}


pub struct BitStreamW{
    data: Vec<u8>,
    byte: usize,
    b_off: usize,
    flushed: bool,
    buf: u8,
    le: bool,
    pub context: Context,
    pub shared: Option<SharedState>,
}

#[allow(dead_code)]
impl BitStreamW{
    pub fn new()->BitStreamW{
        BitStreamW{ data: Vec::new(), byte: 0, b_off: 0,
        flushed: false, buf: 0, le: false, context: Context::Server, shared: None}
    }
    pub fn new_le()->BitStreamW{
        BitStreamW{ le: true, .. Self::new() }
    }

    pub fn len(&self)->usize{ self.data.len() }
    pub fn pos(&self)->usize{ self.byte*8 + self.b_off }

    pub fn data(mut self)->Vec<u8>{
        self.write_buf();
        self.data
    }

    pub fn write_buf(&mut self){
        if self.flushed { return }
        self.data.push(self.buf);
        self.byte += 1;   self.b_off = 0;
        self.buf = 0;     self.flushed = true;
    }

    pub fn write(&mut self, size: usize, mut v: u64){
        if size > 64 {
            panic!("BitStreamW::write size must be <= 64 bits");
        }
        if self.le && size > 8{
            let b = ((size - 1) / 8) + 1;
            let mut bytes = Vec::new();
            for i in 0 .. b{ bytes.push((v >> (8*i))as u8); }
            v = 0u64;
            for i in 0 .. b{ v |= (bytes[i] as u64) << (8*(b-i-1)); }
        }
        let mut left = size;
        while left > 0 {
            let x = cmp::min(left, 8-self.b_off);
            self.buf |= (((v >> (size - left)) & !((-1i64 as u64) << x))
                         << self.b_off) as u8;
            self.b_off += x;
            left -= x;
            self.flushed = false;
            if self.b_off == 8 {self.write_buf();}
        }
    }

    pub fn write_bool(&mut self, b: bool){ self.write(1, if b {1} else {0}); }

    pub fn write_u64(&mut self, v: u64){ self.write(64,v); }
    pub fn write_u32(&mut self, v: u32){ self.write(32,v as u64); }
    pub fn write_u16(&mut self, v: u16){ self.write(16,v as u64); }
    pub fn write_u8 (&mut self, v: u8 ){ self.write(8 ,v as u64); }

    pub fn write_i64(&mut self, v: i64){ self.write(64,v as u64); }
    pub fn write_i32(&mut self, v: i32){ self.write(32,v as u64); }
    pub fn write_i16(&mut self, v: i16){ self.write(16,v as u64); }
    pub fn write_i8 (&mut self, v: i8 ){ self.write(8 ,v as u64); }

    pub fn write_f64(&mut self, v: f64){
        self.write(64, unsafe{ mem::transmute(v) });
    }
    pub fn write_f32(&mut self, v: f32){
        self.write(32, unsafe{ mem::transmute::<f32,u32>(v) as u64 });
    }

    pub fn write_bytes(&mut self, v: &Vec<u8>){
        for b in v{ self.write_u8(*b); }
    }
    pub fn write_data(&mut self, v: &Vec<u8>, mut bits: usize){
        let mut i = 0;
        while bits > 8{
            self.write_u8(v[i]);
            bits -= 8;
            i += 1;
            if i == v.len() { error!("not enough data!"); return; }
        }
        if bits > 0 { self.write(bits, v[i] as u64); }
    }

    pub fn write_range(&mut self, min: i32, max: i32, v: i32){
        if min >= max { error!("read_range min >= max o.O"); }
        let mut v = v;
        if v < min {
            error!("write_range, value less than min {}->{} {}",min,max,v);
            v = min;
        }
        if v > max {
            error!("write_range, value more than max {}->{} {}",min,max,v);
            v = max;
        }
        let b = bits((max-min) as u32);
        self.write(b, (v - min) as u64);
    }

    pub fn write_srange(&mut self, min: f32, max: f32, scale: f32, v: f32){
        let a = (min * scale + 0.5) as i32;
        let b = (max * scale + 0.5) as i32;
        let v = (v   * scale + 0.5) as i32;
        self.write_range(a,b,v);
    }

    pub fn write_str(&mut self, s: &str){
        self.write_u16(s.len() as u16);
        for b in s.bytes(){
            self.write_u8(b);
        }
    }
    pub fn write_str_l(&mut self, s: &str){
        self.write_u32(s.len() as u32);
        for b in s.bytes(){
            self.write_u8(b);
        }
    }

}

#[cfg(test)]
mod tests{
    use super::BitStreamR;

    fn test_data() -> Vec<u8> { vec![0x34,0xB3,0x2D] }

    #[test] fn in_len(){
        let p = BitStreamR::new(test_data());
        assert_eq!(p.len(), 3);
    }

    #[test] fn in_read(){
        let mut p = BitStreamR::new(test_data());
        assert_eq!(p.read(4), 4);
        assert_eq!(p.read(4), 3);
    }

    #[test] fn in_pos(){
        let mut p = BitStreamR::new(test_data());
        let _ = p.read(3);  assert_eq!(p.pos(), 3);
        let _ = p.read(10); assert_eq!(p.pos(), 13);
    }

    #[test] fn in_seek(){
        let mut p = BitStreamR::new(test_data());
        let _ = p.read(10);
        p.seek(13);
        assert_eq!(p.pos(), 13);
    }

    #[test] fn in_ignore(){
        let mut p = BitStreamR::new(test_data());
        let _ = p.read(3);
        p.ignore(10);
        assert_eq!(p.pos(), 13);
    }
}
