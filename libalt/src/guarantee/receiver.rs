use std::collections::{HashMap,HashSet};
use time::{Timespec,get_time};
use mioco::sync::mpsc;

use super::{Level,Data};
use super::{disp_send,dispatch,get_seq};
use message::*;
use dispatcher::DispMsg;

pub struct Receiver{
    pub id:       i32,
    pub last_rec: Timespec,
    to_disp:      mpsc::Sender<DispMsg>,
    last_seq:     u32,
    last_disp:    u32,
    missing:      HashSet<u32>,
    order_buf:    HashMap<u32,Msg>,
}

impl Receiver{
    pub fn new(c: mpsc::Sender<DispMsg>, id: i32) -> Receiver{
        Receiver{
            id:        id,
            last_rec:  get_time(),
            to_disp:   c,
            last_seq:  0,
            last_disp: 0,
            missing:   HashSet::new(),
            order_buf: HashMap::new(),
        }
    }
    pub fn receive(&mut self, msg: Msg, _seq: u16){
        let seq = get_seq(_seq,self.last_seq);
        self.last_rec = get_time();

        debug!("sending data ack for {}",seq);
        let mut m = msg.reply(EmptyData{});
        m.guarantee.data = Data::DataAck{ seq: _seq };
        disp_send(&self.to_disp, m);

        let is_new = seq > self.last_seq;
        let was_missing = self.missing.contains(&seq);
        if is_new{
            for i in self.last_seq+1 .. seq{
                debug!("waiting for seq {}",i);
                self.missing.insert(i);
            }
            self.last_seq = seq;
        } else if was_missing {
            self.missing.remove(&seq);
        }

        if is_new || was_missing {
            match msg.guarantee.level{
                Level::Delivery =>{
                    dispatch(&self.to_disp, msg);
                },
                Level::Order =>{
                    if self.last_disp + 1 == seq{
                        dispatch(&self.to_disp, msg);
                        self.last_disp += 1;
                    } else {
                        self.order_buf.insert(seq,msg);
                    }
                    loop{
                        let i = self.last_disp + 1;
                        if !self.order_buf.contains_key(&i){break;}
                        let m = self.order_buf.remove(&i)
                           .expect("failed to move from order_buf");
                        dispatch(&self.to_disp, m);
                        self.last_disp += 1;
                    }
                },
                _ => { },
            }
        }
    }
}
