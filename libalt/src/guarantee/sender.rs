use std::collections::HashMap;
use mioco::sync::mpsc;
use time::*;
use rand;

use super::{Guarantee,Data};
use super::{get_seq,make_seq};
use message::*;
use dispatcher::DispMsg;
use socket::SocketMsg;

struct SendData{
    last_send:  Timespec,
    attempts:   i64,
    message:    SocketMsg,
}

pub struct Sender{
    pub open:      bool,
    pub last_send: Timespec,
    pub created:   Timespec,
    pub id:        i32,
    to_disp:       mpsc::Sender<DispMsg>,
    queue:         HashMap<u32,SendData>,
    seq:           u32,
}

impl Sender{
    pub fn new(s: mpsc::Sender<DispMsg>) -> Sender{
        let mut id = 0i32;
        while (id >= 0) && (id <= 16) { id = rand::random(); }
        Sender{
            open:      false,
            last_send: get_time(),
            created:   get_time(),
            id:        id,
            to_disp:   s,
            queue:     HashMap::new(),
            seq:       0u32,
        }
    }

    pub fn ack(&mut self, seq: u16){
        let seq = get_seq(seq, self.seq);
        self.queue.remove(&seq);
    }

    pub fn update(&mut self) -> bool{
        let mut to_remove = Vec::new();
        let mut to_send = Vec::new();
        let time = get_time();
        for (seq,m) in &self.queue{
            if time - m.last_send > Duration::seconds(5){
                to_remove.push(*seq); continue;
            }
            let since_sent = time - m.last_send;
            if since_sent > Duration::milliseconds(400 * m.attempts){
                to_send.push(*seq);
            }
        }
        for s in to_remove{ self.queue.remove(&s); }
        for s in to_send { self._send(s); }
        if self.queue.len() == 0 { false } else { true }
    }

    pub fn send(&mut self, mut msg: Msg, open: bool){
        self.seq += 1;
        let g = Data::Data{seq: make_seq(self.seq)};
        if !self.open{
            let o = Data::Open{ id: self.id, is_open: open,
                data: Box::new(g) };
            msg.guarantee = Guarantee{ level: msg.guarantee.level, data: o};
        } else{
            msg.guarantee = Guarantee{ level: msg.guarantee.level, data: g};
        }
        self.queue.insert(self.seq, SendData{
            last_send:  get_time(),
            attempts:   0,
            message:    SocketMsg::new(msg),
        });
        let _s = self.seq;
        self._send(_s);
    }

    fn _send(&mut self, seq: u32){
        if let Some(data) = self.queue.get_mut(&seq){
            self.last_send = get_time();
            data.last_send = get_time();
            data.attempts += 1;
            debug!("sending guaranteed data, seq: {}", seq);
            self.to_disp.send(DispMsg::OutRaw(data.message.clone())).unwrap_or_else(
                |x| warn!("failed to send to dispatcher: {}",x));
        }
    }
}
