//! An implementation of altitude's transport protocol.
//!
//! Altitude uses UDP, with optional guarantees for message deliver
//! and order. This module contains a representation of the
//! guarantee data, and a guarantee manager, which sorts incoming
//! and outgoing messages.

use ::net::netable::Netable;
use ::packet::*;

mod manager;
mod sender;
mod receiver;

pub use self::manager::{Manager,GMMsg};
pub use self::sender::Sender;
pub use self::receiver::Receiver;

#[derive(Clone,Copy,Debug)]
pub enum Level{ None = 0, Delivery, Order, Ping}

#[derive(Clone,Debug)]
pub enum Data{
    Open{
        id:      i32,
        is_open: bool,
        data:    Box<Data>,
    },
    OpenAck{ id:  i32},
    Data{    seq: u16},
    DataAck{ seq: u16 },
    None,
}

#[derive(Clone,Debug)]
pub struct Guarantee{
    pub level: Level,
    pub data: Data,
}

impl Netable for Level{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let t = try!(p.read(2));
        Ok(match t{
            0 => Level::None,  1 => Level::Delivery,
            2 => Level::Order, 3 => Level::Ping,
            _ => unreachable!(),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(2, *self as u64);
    }
}

impl Netable for Data{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let t = try!(p.read(2));
        Ok(match t{
            0 => Data::Open{
                id:      try!(p.read_i32()),
                is_open: try!(p.read_bool()),
                data:    Box::new(try!(Data::read(p)))},
            1 => Data::OpenAck{ id:  try!(p.read_i32())},
            2 => Data::Data{    seq: try!(p.read_u16())},
            3 => Data::DataAck{ seq: try!(p.read_u16())},
            _ => unreachable!(),
        })
    }

    fn write(&self, p: &mut BitStreamW){
        match *self{
            Data::Open{ref id, ref is_open, ref data} =>{
                p.write(2, 0);
                p.write_i32(*id);
                p.write_bool(*is_open);
                data.write(p);
            },
            Data::OpenAck{ ref id }  => { p.write(2, 1); p.write_i32(*id); }
            Data::Data{ ref seq }    => { p.write(2, 2); p.write_u16(*seq);}
            Data::DataAck{ ref seq } => { p.write(2, 3); p.write_u16(*seq);}
            Data::None => {}
        }
    }
}

use dispatcher::DispMsg;
use message::*;
use socket::SocketMsg;
use mioco::sync::mpsc;

pub fn disp_send<T>(sender: &mpsc::Sender<DispMsg>, m: Message<T>)
where T: MsgData{
    sender.send(DispMsg::OutRaw(SocketMsg::new(m.upcast()))).unwrap_or_else(
        |x| warn!("failed to send to dispatcher {}", x));
}

pub fn dispatch(sender: &mpsc::Sender<DispMsg>, m: Msg){
    sender.send(DispMsg::InRaw(m)).unwrap_or_else(
        |x| warn!("failed to send to dispatcher {}", x));
}

pub fn get_seq(seq: u16, prev: u32) -> u32{
    let mut seq = seq as u32;
    let a = 1 << 16;
    while seq < prev { seq += a }
    if seq > prev + a/2 { seq -= a }
    seq
}
pub fn make_seq(seq: u32) -> u16{ seq as u16 }
