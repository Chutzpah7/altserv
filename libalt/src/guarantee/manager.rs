use std::net::SocketAddr;
use std::collections::HashMap;
use std::cell::{RefCell,RefMut};
use mioco;
use mioco::sync::mpsc;
use mioco::timer::Timer;
use time::{Duration,get_time};

use super::{Level,Data};
use super::{disp_send,Sender,Receiver};
use dispatcher::DispMsg;
use message::*;

const UPDATE_MS: i64 = 10;
const CLEAN_MS:  i64 = 5000;

pub enum GMMsg{
    Out(Msg),
    In(Msg)
}

pub struct Manager{
    to_disp:   mpsc::Sender<DispMsg>,
    senders:   HashMap<SocketAddr, RefCell<Sender>>,
    receivers: HashMap<SocketAddr, RefCell<Receiver>>,
    sending:   bool,
    empty:     bool,
}

impl Manager{
    pub fn new(c: mpsc::Sender<DispMsg>) -> Manager{
        Manager{
            to_disp:   c,
            senders:   HashMap::new(),
            receivers: HashMap::new(),
            sending:   false,
            empty:     true,
        }
    }
    pub fn start(c: mpsc::Sender<DispMsg>) -> mpsc::Sender<GMMsg>{
        let (sx, rx) = mpsc::channel();
        let mgr = Manager::new(c);
        mioco::spawn(move || mgr.run(rx));
        sx
    }
    fn run(mut self, rx: mpsc::Receiver<GMMsg>){
        let mut update_timer = Timer::new();
        let mut clean_timer = Timer::new();
        use mioco::Evented;
        debug!("starting guarantee manager");
        loop{
            unsafe{
                rx.select_add(mioco::RW::read());
                if self.sending{ update_timer.select_add(mioco::RW::read()); }
                if !self.empty{ clean_timer.select_add(mioco::RW::read()); }
            }
            let ret = mioco::select_wait();
            if !self.empty && ret.id() == clean_timer.id(){
                self.clean();
                clean_timer.set_timeout(CLEAN_MS);
            }
            if self.sending && ret.id() == update_timer.id(){
                self.update();
                update_timer.set_timeout(UPDATE_MS);
            }
            if ret.id() == rx.id(){
                if let Ok(msg) = rx.try_recv(){
                    self.from_dispatcher(msg);
                }
            }
        }
    }
}

impl Manager{
    fn from_dispatcher(&mut self, msg: GMMsg){
        match msg{
            GMMsg::In(x) => self.receive(x),
            GMMsg::Out(x) => self.send(x),
        }
    }

    fn update(&mut self){
        let time = get_time();
        let mut to_remove = Vec::new();

        self.sending = false;
        for (addr, s) in &mut self.senders{
            let mut s = s.borrow_mut();
            let sending = s.update();
            if !sending && time - s.last_send > Duration::seconds(20){
                to_remove.push(addr.clone());
            }
            self.sending |= sending;
        }
        for ref s in to_remove{ self.senders.remove(s); }
    }

    fn receive(&mut self, msg: Msg){
        let data = msg.guarantee.data.clone();
        match msg.guarantee.level{
            Level::Delivery | Level::Order =>
                self.handle(msg,data),
            _ => warn!("gmgr received non-guaranteed message"),
        }
    }

    fn send(&mut self, msg: Msg){
        let has_rec = self.get_receiver(&msg).is_some();
        if let Some(mut sender) = self.get_sender(&msg){
            sender.send(msg,has_rec);
            return;
        }

        debug!("creating a new sender for {}",msg.addr);
        self.empty = false;
        let addr = msg.addr;
        self.senders.insert(addr.clone(),
            RefCell::new(Sender::new(self.to_disp.clone())));

        {
            let mut sender = self.get_sender(&msg)
                .expect("failed to create sender");
            sender.send(msg,has_rec);
        }

        self.sending = true;
    }

    fn handle(&mut self, msg: Msg, gdata: Data){
        match gdata {
            Data::Open{id, is_open, data} => self.open(msg, id, is_open, data),
            Data::OpenAck{id} => self.open_ack(&msg, id),
            Data::Data{seq} => self.data(msg, seq),
            Data::DataAck{seq} => self.data_ack(&msg, seq),
            _ =>{ warn!("no guarantee data, ignoring packet."); }
        }
    }

    fn open(&mut self, msg: Msg, id: i32, is_open: bool,
    data: Box<Data>){
        debug!("got open request from {}: id: {}, is_open: {}",
               msg.addr, id, is_open);

        debug!("sending open ack, id: {}", id);
        let mut m = msg.reply(EmptyData{});
        m.guarantee.data = Data::OpenAck{ id: id };
        disp_send(&self.to_disp, m);

        let has_rec = self.get_receiver(&msg).is_some();
        let has_snd = self.get_sender(&msg).is_some();

        if !has_rec || self.get_receiver(&msg).expect("wat1").id != id {
            self.empty = false;
            self.receivers.insert(msg.addr.clone(), RefCell::new(
                Receiver::new(self.to_disp.clone(), id)));

            // If we have an old sender and they have no receiver,
            // delete the sender.
            if has_snd & !is_open {
                if get_time() - self.get_sender(&msg).expect("wat2").created >
                        Duration::seconds(5){
                    debug!("removing old sender for {}", msg.addr);
                    self.senders.remove(&msg.addr);
                }
            }
        }
        self.handle(msg, data.as_ref().clone());
    }
    fn open_ack(&mut self, msg: &Msg, id: i32){
        debug!("got open ack for {}", id);
        if id == 1{
            self.senders.remove(&msg.addr);
            self.receivers.remove(&msg.addr);
        }
        if let Some(mut s) = self.get_sender(msg){
            if s.id == id { s.open = true; }
        }
    }
    fn data(&mut self, msg: Msg, seq: u16){
        if let Some(mut receiver) = self.get_receiver(&msg){
            debug!("got data, seq: {}",seq);
            receiver.receive(msg,seq);
        }
        else{
            debug!("got data without a connection, sending openack(1)");
            let mut m = msg.reply(EmptyData{});
            m.guarantee.data = Data::OpenAck{ id: 1 };
            disp_send(&self.to_disp, m);
        }
    }
    fn data_ack(&mut self, msg: &Msg, seq: u16){
        debug!("got data ack for {}",seq);
        if let Some(mut s) = self.get_sender(msg){ s.ack(seq); }
    }


    fn get_sender(&self, msg: &Msg) -> Option<RefMut<Sender>>{
        if let Some(ret) = self.senders.get(&msg.addr){
            Some(ret.borrow_mut())
        } else { None }
    }
    fn get_receiver(&self, msg: &Msg) -> Option<RefMut<Receiver>>{
        if let Some(ret) = self.receivers.get(&msg.addr) {
            Some(ret.borrow_mut())
         } else { None }
    }

    fn clean(&mut self){
        let time = get_time();
        let mut old_senders = Vec::new();
        let mut old_receivers = Vec::new();
        for (addr,s) in &self.senders{
            let s = s.borrow();
            if time - s.last_send > Duration::seconds(20){
                old_senders.push(addr.clone());
                debug!("removing old sender for {}", addr);
            }
        }
        for (addr,r) in &self.receivers{
            let r = r.borrow();
            if time - r.last_rec> Duration::seconds(36){
                old_receivers.push(addr.clone());
                debug!("removing old receiver for {}", addr);
            }
        }
        for s in &old_senders{ self.senders.remove(s); }
        for r in &old_receivers{ self.receivers.remove(r); }

        if self.senders.len() == 0 && self.receivers.len() == 0 {
            self.empty = true; }
    }
}
