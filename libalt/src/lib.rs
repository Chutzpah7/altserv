//! An implementation of the networkinng protocol of Altitude, a competitive
//! 2D plane game.
//!
//! ```ignore
//! let mut disp = Dispatcher::new();
//! let sx = disp.sender();
//! let rx = disp.receiver::<PingMsg>(27270);
//!
//! mioco::spawn(move || disp.run());
//!
//! loop{
//!     let ping = Message::new("127.0.0.1:27276",
//!                 27270, guarantee::Level::Ping,
//!                 PingMsg{ id: 0, respond: true });
//!     send(&sx, ping);
//!     let reply = recv::<PingMsg>(&rx).unwrap();
//!     mioco::sleep_ms(1000);
//! }
//! ```

#[macro_use] extern crate log;
#[macro_use] extern crate mioco;
#[macro_use] extern crate mopa;
extern crate mio;
extern crate time;
extern crate rand;

extern crate math;

pub mod packet;
pub mod socket;
pub mod state;
pub mod message;
pub mod guarantee;

pub mod dispatcher;
pub mod net;
pub mod event;
pub mod entity;
pub mod event_mgr;

pub fn mioco_conf() -> mioco::Config{
    let mut mioco_conf = mioco::Config::new();
    mioco_conf.event_loop().timer_tick_ms(15u64);
    mioco_conf.set_catch_panics(false);
    mioco_conf
}
