//! A manager and sequencer for game events.

#![allow(unused_variables)]

use std::net::SocketAddr;
use std::collections::VecDeque;
use mioco::sync::mpsc::*;
use event::{Event,get_seq};
use event::ev_group::*;
use message::*;
use net::game_msg::*;
use dispatcher::send;
use guarantee::Level;

pub struct EventMgr{
    sx:           Sender<Msg>,
    addr:         SocketAddr,
    port:         u16,
    pub counter:      u8,
    send:         VecDeque<Event>,
    recv:         VecDeque<Event>,
    send_seq:     u64,
    group_seq:    u64,
    in_group_seq: u64,
    connected:    bool,
}

impl EventMgr{
    pub fn new(sx: Sender<Msg>, addr: SocketAddr, port: u16) -> EventMgr{
        EventMgr{
            sx:           sx,
            addr:         addr,
            port:         port,
            counter:      0,
            send:         VecDeque::new(),
            recv:         VecDeque::new(),
            send_seq:     0,
            group_seq:    0,
            in_group_seq: 0,
            connected:    false,
        }
    }
    pub fn send(&mut self, ev: Event){
        self.send.push_back(ev);
    }
    pub fn next(&mut self) -> Option<Event>{
        self.recv.pop_front()
    }
    pub fn tick(&mut self){
        let mut msg = GameMsg::new();
        msg.counter = self.counter;
        msg.seq = self.send_seq as u16;
        msg.group_id = self.group_seq as u16;
        if self.send.len() > 0 {
            let mut group = EvGroup::new(self.group_seq as u16);
            while let Some(ev) = self.send.pop_front(){
                group.events.push(ev);
            }
            msg.ev_groups.push(group);
            self.group_seq += 1;
        }
        self.send_msg(msg);
        self.send_seq += 1;
    }

    pub fn msg_in(&mut self, msg: Box<GameMsg>){
        for group in msg.ev_groups{
            let seq = get_seq(group.id as u64,10,self.in_group_seq);
            if seq > self.in_group_seq || !self.connected {
                self.connected = true;
                self.in_group_seq = seq;
                for ev in group.events{
                    self.recv.push_back(ev);
                }
            }
        }
    }
    pub fn inc_counter(&mut self) -> u8{
        self.counter += 1;
        if self.counter == 4 { self.counter = 0; }
        self.counter
    }

    fn send_msg(&mut self, msg: GameMsg){
        let m = Message::new(self.addr.clone(), self.port, Level::None, msg);
        let _ = send(&self.sx,m);
    }
}
