use mioco;
use mioco::sync::mpsc::*;
use std::sync::mpsc::{TryRecvError,SendError,RecvError};
use message::*;
use state::*;
use packet::Context;
use guarantee::{GMMsg,Manager};
use socket::{self,SocketMsg};
use std::any::TypeId;
use mopa::Any;
use std::collections::{HashMap,HashSet};


#[derive(Debug)]
pub enum RecvErr{
    ReceiverError(RecvError),
    DowncastError(Msg),
}

#[derive(Debug)]
pub enum TryRecvErr{
    ReceiverError(TryRecvError),
    DowncastError(Msg),
}

/// A non-blocking send on a message sender for any type of message.
///
/// upcasts's the message to a Message<MsgData> and sends over the sender.
pub fn send<T: MsgData>(s: &Sender<Msg>, msg: Message<T>)
-> Result<(),SendError<Msg>>{
    s.send(msg.upcast())
}

/// A blocking receive on a game message channel
///
/// Returns a message of the specified type if sucessful, if not
/// (if the socket is closed, or something internally has gone really wrong)
/// it returns the error.
///
/// It's *probably* safeish to `unwrap` this, because if it's an Err you're
/// pretty much screwed anyway :3
pub fn recv<T: MsgData>(r: &Receiver<Msg>) -> Result<Message<T>,RecvErr>{
    match r.recv(){
        Ok(msg) => {
            match msg.downcast::<T>(){
                Ok(msg) => Ok(msg),
                Err(e) => Err(RecvErr::DowncastError(e)),
            }
        },
        Err(e) => Err(RecvErr::ReceiverError(e)),
    }
}

/// A non-blocking receive on a game message channel
///
/// Like recv, but doesn't block. The error type has an extra value for Empty.
pub fn try_recv<T: MsgData>(r: &Receiver<Msg>) -> Result<Message<T>,TryRecvErr>{
    match r.try_recv(){
        Ok(msg) => {
            match msg.downcast::<T>(){
                Ok(msg) => Ok(msg),
                Err(e) => Err(TryRecvErr::DowncastError(e)),
            }
        },
        Err(e) => Err(TryRecvErr::ReceiverError(e)),
    }
}

/// A message sent to the dispatcher, mostly used internally.
#[derive(Debug)]
pub enum DispMsg{
    In(Msg),
    InRaw(Msg),
    OutRaw(SocketMsg),
}

/// Sends and receives altitude messages
pub struct Dispatcher{
    // Dispatcher -> Handlers
    senders:   HashMap<(u16,TypeId), Sender<Msg>>,
    ports:     HashSet<u16>,
    // Handlers -> Dispatcher
    msg_sx:    Sender<Msg>,
    msg_rx:    Option<Receiver<Msg>>,
    // Dispatcher -> Socket
    socket_sx: HashMap<u16,Sender<SocketMsg>>,
    // Socket -> Dispatcher
    disp_sx:   Sender<DispMsg>,
    disp_rx:   Option<Receiver<DispMsg>>,
    // Guarantee manager
    guarantee: Sender<GMMsg>,
    pub context_in: Context,
    pub context_out: Context,
    pub shared: SharedState,
}

impl Dispatcher{
    /// Creates a new dispatcher.
    pub fn new() -> Dispatcher{
        let (msg_sx,msg_rx) = channel();
        let (disp_sx,disp_rx) = channel();
        Dispatcher{
            senders:     HashMap::new(),
            ports:       HashSet::new(),
            msg_sx:      msg_sx,
            msg_rx:      Some(msg_rx),
            socket_sx:   HashMap::new(),
            disp_sx:     disp_sx.clone(),
            disp_rx:     Some(disp_rx),
            guarantee:   Manager::start(disp_sx),
            context_in:  Context::Server,
            context_out: Context::Server,
            shared:      SharedState::new(),
        }
    }

    /// Runs the dispatcher.
    /// This blocks the thread, so it's proabbly best to call it in a `mioco::spawn`
    pub fn run(self){
        self.shared.write().load_maps("maps");
        let msg_rx = self.msg_rx.unwrap();
        let disp_rx = self.disp_rx.unwrap();
        let mut disp = Dispatcher{
            msg_rx: None, disp_rx: None, .. self};
        loop{
            select!(
                r:msg_rx => {
                    if let Ok(msg) = msg_rx.try_recv(){
                        disp.guarantee_out(msg);
                    }
                },
                r:disp_rx => {
                    // Dispatch message to handler
                    if let Ok(msg) = disp_rx.try_recv(){
                        match msg{
                            DispMsg::In(m)     => disp.guarantee_in(m),
                            DispMsg::InRaw(m)  => disp.dispatch(m),
                            DispMsg::OutRaw(m) => disp.send(m),
                        }
                    }
                },
                );
        }
    }

    /// Creates a new sender to send messages to clients
    pub fn sender(&self) -> Sender<Msg>{
        self.msg_sx.clone()
    }

    /// Creates a receiver of a specific type, to receive messages from clients.
    pub fn receiver<T: MsgData>(&mut self, port: u16) -> Receiver<Msg>{
        if !self.ports.contains(&port){ self.add_socket::<T>(port); }
        let (sx,rx) = channel();
        self.senders.insert((port,TypeId::of::<T>()),sx);
        rx
    }

    /// Creates a receiver to receive any type of message which is not taken
    /// by another receiver.
    pub fn catchall(&mut self, port: u16) -> Receiver<Msg>{
        if !self.ports.contains(&port){ self.add_socket::<EmptyData>(port); }
        let (sx,rx) = channel();
        self.senders.insert((port,TypeId::of::<MsgData>()),sx);
        rx
    }

    fn add_socket<T: MsgData>(&mut self, port: u16){
        use std::any::TypeId;
        let multicast = TypeId::of::<T>() ==
                        TypeId::of::<::net::lan_msg::LanMsg>();
        // if T::is<VaporMsg>(){ //glhf }
        self.ports.insert(port);
        let sx = socket::game(self.disp_sx.clone(),port,multicast,
                              self.context_in, self.context_out,
                              self.shared.clone());
        self.socket_sx.insert(port, sx);
    }


    fn guarantee_in(&mut self, msg: Msg){
        use guarantee::Level;
        match msg.guarantee.level{
            Level::Delivery | Level::Order =>
                self.guarantee.send(GMMsg::In(msg)).unwrap_or_else(|e|
                    warn!("Failed to pass message to guarantee manager: {}",e)),
            _ => self.dispatch(msg),
        }
    }
    fn guarantee_out(&self, msg: Msg){
        use guarantee::Level;
        match msg.guarantee.level{
            Level::Delivery | Level::Order =>
                self.guarantee.send(GMMsg::Out(msg)).unwrap_or_else(|e|
                    warn!("Failed to pass message to guarantee manager: {}",e)),
            _ => self.send(SocketMsg::new(msg)),
        }
    }

    fn dispatch(&mut self, msg: Msg){
        if Any::get_type_id(msg.data.as_ref()) == TypeId::of::<EmptyData>(){
            return;}
        let mut remove = false;
        let x = &(msg.port,Any::get_type_id(msg.data.as_ref()));
        match self.senders.get(&x){
            Some(h) => {
                if h.send(msg).is_err(){
                    warn!("Receiver is broken, removing sender");
                    remove = true;
                };
            }
            None => {
                if let Some(h) = self.senders.get(
                        &(msg.port, TypeId::of::<MsgData>())){
                    if h.send(msg).is_err(){
                        warn!("Receiver is broken, removing sender");
                        remove = true;
                    };
                } else {
                    warn!("No handler exists for this message: {:?}", msg);
                    warn!("known handlers: {:?}", self.senders.keys()
                          .collect::<Vec<&(u16,TypeId)>>());
                    warn!("this msg type: {:?}", x);
                }
            }
        }
        if remove { self.senders.remove(&x); }
    }

    fn send(&self, msg: SocketMsg){
        let port = msg.get().port;
        match self.socket_sx.get(&port){
            Some(sender) => {
                if sender.send(msg).is_err() {
                    error!("socket sender for port {} broken!", port);
                    panic!("socket sender broken - probably a bug.");
                }
            },
            None => warn!("No sender for message {:?}",*msg.get()),
        }
    }
}
