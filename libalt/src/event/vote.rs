use net::netable::*;
use packet::*;

#[derive(Debug,Clone)]
pub struct VoteStartEv{
    strings_a: Vec<String>,
    ints_a:    Vec<i32>,
    strings_b: Vec<String>,
    ints_b:    Vec<i32>,
    message:   String,
    options:   Vec<String>,
    bool_a:    bool,
    bool_b:    bool,
}

#[derive(Debug,Clone)]
pub struct VoteCastEv{
    message: String,
    value: u8,
}

#[derive(Debug,Clone)]
pub struct VoteEndEv{
    message: String,
}

impl Netable for VoteStartEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(VoteStartEv{
            strings_a: try!(Vec::<String>::read(p)),
            ints_a:    try!(Vec::<i32>::read(p)),
            strings_b: try!(Vec::<String>::read(p)),
            ints_b:    try!(Vec::<i32>::read(p)),
            message:   try!(p.read_str()),
            options:   try!(Vec::<String>::read(p)),
            bool_a:    try!(p.read_bool()),
            bool_b:    try!(p.read_bool()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.strings_a.write(p);
        self.ints_a.write(p);
        self.strings_b.write(p);
        self.ints_b.write(p);
        p.write_str(&self.message);
        self.options.write(p);
        p.write_bool(self.bool_a);
        p.write_bool(self.bool_b);
    }
}

impl Netable for VoteCastEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(VoteCastEv{
            message: try!(p.read_str()),
            value: try!(p.read_u8()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_str(&self.message);
        p.write_u8(self.value);
    }
}

impl Netable for VoteEndEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(VoteEndEv{
            message: try!(p.read_str()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_str(&self.message);
    }
}

