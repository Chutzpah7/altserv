use packet::*;
use net::netable::*;
use event::Event;

#[derive(Debug,Clone)]
pub struct EvGroup{
    pub id: u16,
    pub events: Vec<Event>,
    pub b: bool,
}

impl EvGroup{
    pub fn new(id: u16) -> EvGroup{
        EvGroup{
            id: id,
            events: Vec::new(),
            b: false,
        }
    }
    pub fn events<F: Fn(&mut Event)>(&mut self, f: F){
        for ev in &mut self.events{ f(ev); }
    }
}

impl Netable for EvGroup{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let id = try!(p.read(10)) as u16;
        let mut events = Vec::new();

        let len = try!(p.read(11));
        let end_pos = p.pos() + len as usize;

        let b = try!(p.read_bool());
        if b {
            return Err(Unimplemented);
        } else {
            while p.pos() < end_pos{
                events.push(try!(Event::read(p)));
            }
        }
        try!(p.seek(end_pos));
        Ok(EvGroup{
            id: id,
            events: events,
            b: b,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(10,self.id as u64);

        let mut p2 = BitStreamW::new();
        p2.context = p.context;
        p2.shared = p.shared.clone();

        if self.b { unimplemented!(); }
        else{
            p2.write_bool(false);
            for e in &self.events{
                e.write(&mut p2);
            }
        }
        let bits = p2.pos();
        let data = p2.data();

        p.write(11,bits as u64);
        p.write_data(&data,bits);
    }
}
