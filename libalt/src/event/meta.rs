use net::netable::*;
use packet::*;
use net::server_conf::ServerConf;
use math::Vec2f;
use entity::{write_map_pos, read_map_pos};
use event::get_seq;

#[derive(Debug,Clone)]
pub struct ServerConfigEv{
    is_compressed: bool,
    conf: ServerConf,
}

#[derive(Debug,Clone)]
pub struct PingsEv{ data: Vec<u8>, }

#[derive(Debug,Clone)]
pub struct ViewEv{ pub pos: Vec2f, }

#[derive(Debug,Clone)]
pub struct PacketCountEv{ pub x: u16, }


impl Netable for ServerConfigEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let is_compressed = try!(p.read_bool());
        if is_compressed{
            return Err(Unimplemented);
        }
        let pos = p.pos() + try!(p.read(12)) as usize;

        let conf = try!(ServerConf::read(p));

        if p.pos() != pos{
            warn!("ServerConfigEv length not as expected, maybe a parser bug?");
            try!(p.seek(pos));
        }

        Ok(ServerConfigEv{
            is_compressed: is_compressed,
            conf: conf,
        })
    }
    fn write(&self, _p: &mut BitStreamW){
        unimplemented!();
    }
}

impl Netable for PingsEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let len = try!(p.read(12));
        Ok(PingsEv{ data: try!(p.read_bytes(len)) })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(12, self.data.len() as u64);
        p.write_bytes(&self.data);
    }
}

impl Netable for ViewEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(ViewEv{
            pos: try!(read_map_pos(p,0.,1.)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        write_map_pos(p, 0., 1., self.pos);
    }
}

impl Netable for PacketCountEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(PacketCountEv{
            x: get_seq(try!(p.read(10)), 10, 0) as u16,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(10, self.x as u64);
    }
}
