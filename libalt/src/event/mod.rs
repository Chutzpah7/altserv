use net::netable::*;
use packet::*;
use net::plane_setup::*;
use net::team::Team;
use net::playerno::*;
use net::net_id::*;
use net::uuid::Uuid;
use entity::Entity;
use entity::{write_map_pos, read_map_pos};
use math::Vec2f;

pub mod ev_group;
pub mod vote;
pub mod meta;

pub use self::vote::*;
pub use self::meta::*;

#[derive(Debug,Clone)]
pub enum Event{
    NewEntity(EntityEv),
    RemoveEntity(RemoveEv),
    PlayerInfo(PlayerInfoEv),
    EntityMap(EntityMapEv),
    Hit(HitEv),
    TeamSet(TeamEv),
    PowerupPlane(PowerupPlaneEv),
    Chat(ChatEv),
    Pings(PingsEv),
    GameModeStatus(GameModeStatusEv),
    GameMode(GameModeEv),
    ServerConfig(ServerConfigEv),
    Stats(StatsEv),
    View(ViewEv),
    Command(CommandEv),
    CmdResp(CmdRespEv),
    VoteStart(VoteStartEv),
    VoteEnd(VoteEndEv),
    VoteCast(VoteCastEv),
    PacketCount(PacketCountEv),
    SpawnReq(SpawnReqEv),
    SpawnResp(SpawnRespEv),
    Unknown
}
use self::Event::*;

impl Netable for Event{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let header = try!(p.read(6));
        use self::Event::*;
        let ret = match header{
            0  => NewEntity(try!(EntityEv::read(p))),
            1  => RemoveEntity(try!(RemoveEv::read(p))),
            2  => PlayerInfo(try!(PlayerInfoEv::read(p))),
            3  => TeamSet(try!(TeamEv::read(p))),
            4  => EntityMap(try!(EntityMapEv::read(p))),
            8  => Hit(try!(HitEv::read(p))),
            12 => PowerupPlane(try!(PowerupPlaneEv::read(p))),
            13 => Chat(try!(ChatEv::read(p))),
            18 => Pings(try!(PingsEv::read(p))),
            19 => GameModeStatus(try!(GameModeStatusEv::read(p))),
            20 => GameMode(try!(GameModeEv::read(p))),
            21 => ServerConfig(try!(ServerConfigEv::read(p))),
            25 => Stats(try!(StatsEv::read(p))),
            27 => View(try!(ViewEv::read(p))),
            30 => Command(try!(CommandEv::read(p))),
            31 => CmdResp(try!(CmdRespEv::read(p))),
            32 => VoteStart(try!(VoteStartEv::read(p))),
            34 => VoteEnd(try!(VoteEndEv::read(p))),
            35 => VoteCast(try!(VoteCastEv::read(p))),
            36 => PacketCount(try!(PacketCountEv::read(p))),
            38 => SpawnReq(try!(SpawnReqEv::read(p))),
            39 => SpawnResp(try!(SpawnRespEv::read(p))),
            x => {
                error!("unimplmented event read: {}",x);
                return Err(Unimplemented);
            }
        };
        match ret {
            PacketCount(_) | Pings(_) | View(_) => (),
            _ => debug!("Event: {:?}", ret)
        }
        Ok(ret)
    }
    fn write(&self, p: &mut BitStreamW){
        match *self{
            NewEntity(ref x)      => { p.write(6,0); x.write(p); }
            RemoveEntity(ref x)   => { p.write(6,1); x.write(p); }
            PlayerInfo(ref x)     => { p.write(6,2); x.write(p); }
            TeamSet(ref x)        => { p.write(6,3); x.write(p); }
            EntityMap(ref x)      => { p.write(6,4); x.write(p); }
            Hit(ref x)            => { p.write(6,8); x.write(p); }
            PowerupPlane(ref x)   => { p.write(6,12); x.write(p); }
            Chat(ref x)           => { p.write(6,13); x.write(p); }
            Pings(ref x)          => { p.write(6,18); x.write(p); }
            GameModeStatus(ref x) => { p.write(6,19); x.write(p); }
            GameMode(ref x)       => { p.write(6,20); x.write(p); }
            View(ref x)           => { p.write(6,27); x.write(p); }
            Command(ref x)        => { p.write(6,30); x.write(p); }
            CmdResp(ref x)        => { p.write(6,31); x.write(p); }
            VoteStart(ref x)      => { p.write(6,32); x.write(p); }
            VoteEnd(ref x)        => { p.write(6,34); x.write(p); }
            VoteCast(ref x)       => { p.write(6,35); x.write(p); }
            PacketCount(ref x)    => { p.write(6,36); x.write(p); }
            SpawnReq(ref x)       => { p.write(6,38); x.write(p); }
            SpawnResp(ref x)      => { p.write(6,39); x.write(p); }
            ref x                 => {
                error!("unimplemented event write: {:?}",x);
                unimplemented!();
            },
        }
    }
}

#[derive(Debug,Clone)]
pub struct PlaneSetKEv{
    pub plane: NetID,
    pub k: Vec2f,
}

impl Netable for PlaneSetKEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(PlaneSetKEv{
            plane: try!(NetID::read(p)),
            k: {
                let x = try!(p.read_srange(-15.,15.,8.5));
                let y = try!(p.read_srange(-15.,15.,8.5));
                Vec2f::new(x,y)
            }
        })
    }
    fn write(&self, p: &mut BitStreamW){
        unimplemented!();
    }
}

#[derive(Debug,Clone)]
pub struct HitEv{
    pub entity: NetID,
    pub target: NetID,
    pub damage: f32,
    pub angle:  f32,
}

impl Netable for HitEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(HitEv{
            entity: try!(NetID::read(p)),
            target: try!(NetID::read(p)),
            damage: try!(p.read_srange(0., 3276., 10.)),
            angle:  try!(p.read_srange(-180., 180., 0.04347826)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.entity.write(p);
        self.target.write(p);
        p.write_srange(0., 3276., 10., self.damage);
        p.write_srange(-180., 180., 0.04347826, self.angle);
    }
}

#[derive(Debug,Clone)]
pub struct EntityMapEv{
    pub a: u16, pub b: u16
}

impl Netable for EntityMapEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(EntityMapEv{
            a: try!(p.read(10)) as u16,
            b: try!(p.read(10)) as u16,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(10, self.a as u64);
        p.write(10, self.b as u64);
    }
}


#[derive(Debug,Clone)]
pub struct PowerupPlaneEv{
    pub plane: NetID,
    pub powerup: NetID,
}

impl Netable for PowerupPlaneEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(PowerupPlaneEv{
            plane:   try!(NetID::read(p)),
            powerup: try!(NetID::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.plane.write(p);
        self.powerup.write(p);
    }
}


#[derive(Debug,Clone)]
pub struct CommandEv{
    pub command: String,
}

impl Netable for CommandEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(CommandEv{ command: try!(p.read_str()) })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_str(&self.command);
    }
}

#[derive(Debug,Clone)]
pub struct SpawnRespEv{
    pub team:    Team,
    pub setup:   PlaneSetup,
    pub pos:     Vec2f,
    pub angle:   i32,
    pub reason:  String,
    pub timeout: u16,
}

impl Netable for SpawnRespEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(SpawnRespEv{
            team:    try!(Team::read(p)),
            setup:   try!(PlaneSetup::read(p)),
            pos:     { let pos = try!(read_map_pos(p, 50., 1.)); info!("pos: {:?}", pos); pos},
            angle:   try!(p.read_range(-180,180)),
            reason:  try!(p.read_str()),
            timeout: try!(p.read_range(0,450)) as u16,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.team.write(p);
        self.setup.write(p);
        write_map_pos(p, 50., 1., self.pos);
        p.write_range(-180,180,self.angle);
        p.write_str(&self.reason);
        p.write_range(0,450,self.timeout as i32);
    }
}

#[derive(Debug,Clone)]
pub struct SpawnReqEv{
    pub setup:  PlaneSetup,
    pub random: RandomType,
    pub team:   Team,
}

impl Netable for SpawnReqEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(SpawnReqEv{
            setup:  try!(PlaneSetup::read(p)),
            random: try!(RandomType::read(p)),
            team:   try!(Team::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.setup.write(p);
        self.random.write(p);
        self.team.write(p);
    }
}

#[derive(Debug,Clone)]
pub struct EntityEv{
    pub is_a: bool,
    pub id: u16,
    pub entity: Entity,
}

impl Netable for EntityEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(EntityEv{
            is_a:   try!(p.read_bool()),
            id:     try!(p.read(10)) as u16,
            entity: try!(Entity::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_bool(self.is_a);
        p.write(10, self.id as u64);
        self.entity.write(p);
    }
}


#[derive(Debug,Clone)]
pub struct RemoveEv{
    pub id: u16,
}

impl Netable for RemoveEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(RemoveEv{
            id: try!(p.read(10)) as u16,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(10, self.id as u64);
    }
}

#[derive(Debug,Clone)]
pub struct TeamEv{
    pub player: PlayerNo,
    pub team: Team,
}

impl Netable for TeamEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(TeamEv{
            player: try!(PlayerNo::read(p)),
            team:   try!(Team::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.player.write(p);
        self.team.write(p);
    }
}

#[derive(Debug,Clone)]
pub struct GameModeEv{
    data: Vec<u8>,
}

impl Netable for GameModeEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let len = try!(p.read(12));
        Ok(GameModeEv{ data: try!(p.read_bytes(len)) })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(12, self.data.len() as u64);
        p.write_bytes(&self.data);
    }
}

#[derive(Debug,Clone)]
pub struct GameModeStatusEv{
    data: Vec<u8>,
}

impl Netable for GameModeStatusEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let len = try!(p.read(12));
        Ok(GameModeStatusEv{ data: try!(p.read_bytes(len)) })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(12, self.data.len() as u64);
        p.write_bytes(&self.data);
    }
}

#[derive(Debug,Clone)]
pub struct CmdRespEv{
    pub command: String,
    pub result:  String,
}

impl Netable for CmdRespEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(CmdRespEv{
            command: try!(p.read_str()),
            result:  try!(p.read_str()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_str(&self.command);
        p.write_str(&self.result);
    }
}

#[derive(Debug,Clone)]
pub struct PlayerInfoEv{
    pub join: bool,
    pub id: PlayerNo,
    pub reason: Option<String>,
    pub nick: String,
    pub uuid: Uuid,
    pub unk1: i64,
    pub setup: PlaneSetup,
    pub team: Team,
    pub level: u64,
    pub ace: u64,
}

impl Netable for PlayerInfoEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        info!("reading playerinfoev");
        Ok(PlayerInfoEv{
            join:   try!(p.read_bool()),
            id:     try!(PlayerNo::read(p)),
            reason: try!(Option::<String>::read(p)),
            nick:   try!(p.read_str()),
            uuid:   try!(Uuid::read(p)),
            unk1:   try!(p.read_i64()),
            setup:  try!(PlaneSetup::read(p)),
            team:   try!(Team::read(p)),
            level:  try!(p.read_range(1,60)) as u64,
            ace:    try!(p.read(8))
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_bool(self.join);
        self.id.write(p);
        self.reason.write(p);
        p.write_str(&self.nick);
        self.uuid.write(p);
        p.write_i64(self.unk1);
        self.setup.write(p);
        self.team.write(p);
        p.write_range(1,60,self.level as i32);
        p.write(8,self.ace);
    }
}

#[derive(Debug,Clone)]
pub struct StatsEv{ }

impl Netable for StatsEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let len = try!(p.read(12));
        info!("ignoring {} bytes",len);
        try!(p.ignore((len * 8)as usize));
        Ok(StatsEv{})
    }
    fn write(&self, _p: &mut BitStreamW){
        unimplemented!();
    }
}

#[derive(Debug,Clone)]
pub struct ChatEv{
    pub player:  PlayerNo,
    pub is_team: bool,
    pub msg:     String,
}

impl Netable for ChatEv{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(ChatEv{
            player:  try!(PlayerNo::read(p)),
            is_team: try!(p.read_bool()),
            msg:     try!(p.read_str()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.player.write(p);
        p.write_bool(self.is_team);
        p.write_str(&self.msg[..]);
    }
}

/// reads a truncated sequencing number with the given length in bits to the
/// closest origional number, given the last known sequence received.
pub fn get_seq(mut seq: u64, len: u64, prev: u64) -> u64{
    let a = 1u64 << len;
    while seq < prev { seq += a }
    if seq > prev + a/2 && seq > a { seq -= a }
    seq
}
