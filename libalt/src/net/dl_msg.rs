use packet::*;
use ::message::MsgData;

#[derive(Debug,Clone)]
pub enum DlMsg{
    Req{id: u64, name: String},
    Accept{id: u64},
    ReqLost,
    Data,
    Complete,
}

impl MsgData for DlMsg {
    fn header(&self) -> u8 { 72 }
    fn write(&self, p: &mut BitStreamW){
        match *self{
            DlMsg::Req{id, ref name} =>{
                p.write(8,id);
                p.write_str(&name[..]);
            },
            DlMsg::Accept{id} => p.write(8,id),
            _ => unimplemented!(),
        }
    }
}

impl DlMsg{
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        let t = try!(p.read(3));
        Ok(match t{
            0 => DlMsg::Req{
                id:   try!(p.read(8)),
                name: try!(p.read_str())
            },
            1 => DlMsg::Accept{id: try!(p.read(8))},
            2 => DlMsg::ReqLost,
            3 => DlMsg::Data,
            4 => DlMsg::Complete,
            _ => unreachable!(),
        })
    }
}
