use super::netable::*;
use packet::*;
use super::version::*;
use std::net::SocketAddr;

#[derive(Debug,Clone)]
pub struct ServerInfo{
    pub id:            u16,
    pub addr:          Option<SocketAddr>,
    pub map_name:      Option<String>,
    pub max_players:   u16,
    pub num_players:   u16,
    pub server_name:   Option<String>,
    pub pass_req:      bool,
    pub hardcore:      bool,
    pub min_level:     i32,
    pub max_level:     i32,
    pub disallow_demo: bool,
    pub version:       Version,
}

impl Netable for ServerInfo{
    fn read(_p: &mut BitStreamR) -> Res<Self>{
        Err(Unimplemented)
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_u16(self.id);
        self.addr.write(p);
        self.map_name.write(p);
        p.write(8, self.max_players as u64);
        p.write(8, self.num_players as u64);
        self.server_name.write(p);
        p.write_bool(self.pass_req);
        p.write_bool(self.hardcore);
        p.write_range(1,60, self.min_level);
        p.write_range(1,60, self.max_level);
        p.write_bool(self.disallow_demo);
        self.version.write(p);
    }
}

impl Default for ServerInfo{
    fn default() -> Self{
        ServerInfo{
            id:            1,
            addr:          None,
            map_name:      Some("none".to_owned()),
            max_players:   10,
            num_players:   0,
            server_name:   Some("Altserv".to_owned()),
            pass_req:      false,
            hardcore:      true,
            min_level:     1,
            max_level:     60,
            disallow_demo: false,
            version:       VERSION
        }
    }
}
