use packet::*;
use ::message::MsgData;

#[derive(Debug,Clone)]
pub struct PingMsg{
    pub respond: bool,
    pub id: u32,
}
impl MsgData for PingMsg {
    fn header(&self) -> u8 { 73 }
    fn write(&self, p: &mut BitStreamW){
        p.write_bool(self.respond);
        p.write_u32(self.id);
    }
}
impl PingMsg{
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(PingMsg{
            respond: try!(p.read_bool()),
            id:      try!(p.read_u32()),
        })
    }
}

#[derive(Debug,Clone)]
pub struct ServerPingMsg{
    pub respond: bool,
    pub id: u32,
}
impl MsgData for ServerPingMsg {
    fn header(&self) -> u8 { 8 }
    fn write(&self, p: &mut BitStreamW){
        p.write_bool(self.respond);
        p.write_u32(self.id);
    }
}
impl ServerPingMsg{
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(ServerPingMsg{
            respond: try!(p.read_bool()),
            id: try!(p.read_u32()),
        })
    }
}
