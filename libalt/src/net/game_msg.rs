use packet::*;
use super::netable::Netable;
use message::MsgData;
use event::Event;
use event::ev_group::*;

#[derive(Debug,Clone)]
pub struct GameMsg{
    pub counter:   u8,
    pub seq:       u16,
    pub group_id:  u16,
    pub ev_groups: Vec<EvGroup>,
    pub nuons:     Vec<u64>,
    pub data:      Vec<u8>,
    pub data_len:  usize,
    pub valid:     bool,
}

impl MsgData for GameMsg {
    fn header(&self) -> u8 { 71 }
    fn write(&self, p: &mut BitStreamW){
        p.write(2,self.counter as u64);
        p.write(11,self.seq as u64);
        p.write(10,self.group_id as u64);

        if self.valid {
            p.write(2,self.ev_groups.len() as u64);

            for ev in &self.ev_groups{ ev.write(p); }

            p.write(2,0);
            if self.nuons.len() == 0 { p.write_bool(false); }
            else {
                p.write_bool(true);
                p.write(10, self.nuons.len() as u64);
                for n in &self.nuons{
                    p.write(10, *n);
                }
                p.write_data(&self.data, self.data_len);
            }
        } else {
            p.write_data(&self.data, self.data_len);
        }
    }
}

impl GameMsg{
    pub fn new() -> GameMsg{
        GameMsg{
            counter: 0,
            seq: 0,
            group_id: 0,
            ev_groups: Vec::new(),
            nuons: Vec::new(),
            data: Vec::new(),
            data_len: 0,
            valid: true
        }
    }
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        let mut valid = true;
        let mut ev_groups = Vec::new();
        let mut nuons = Vec::new();

        let counter  = try!(p.read(2))  as u8;
        let seq      = try!(p.read(11)) as u16;
        let group_id = try!(p.read(10)) as u16;
        let start_pos = p.pos();

        for _ in 0..try!(p.read(2)){
            match EvGroup::read(p){
                Ok(group) => ev_groups.push(group),
                Err(e) => {
                    valid = false;
                    warn!("Invalid packet: {:?}",e);
                    break;
                }
            }
        }

        if valid {
            for _ in 0..try!(p.read(2)){
                info!("missing ev group {}!", try!(p.read(11)));
            }
        }

        if valid && try!(p.read_bool()) {
            let n = try!(p.read(10));
            for _ in 0..n{
                nuons.push(try!(p.read(10)));
            }
        }

        if !valid { try!(p.seek(start_pos)); }
        let len = p.len() * 8 - p.pos();
        let len: usize = if len > 0 { len as usize } else { 0 };
        let data = try!(p.read_data(len));

        Ok(GameMsg{
            counter: counter,
            seq: seq,
            group_id: group_id,
            ev_groups: ev_groups,
            nuons: nuons,
            data: data,
            data_len: len,
            valid: valid,
        })
    }
    pub fn events<F: Fn(&mut Event)>(&mut self, f: F){
        for group in &mut self.ev_groups{ group.events(&f); }
    }
}

