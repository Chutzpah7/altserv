use net::netable::*;
use packet::*;

// if b, map to/from client netID
#[derive(Debug,Clone)]
pub struct NetID{ id: u16, b: bool }

impl Netable for NetID{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let id = try!(p.read(10)) as u16;
        info!("context: {:?}",p.context);
        let b = if p.context == Context::Server{ try!(p.read_bool()) }
                else { false };
        Ok(NetID{ id: id, b: b })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(10,self.id as u64);
        info!("context: {:?}",p.context);
        if p.context == Context::Client{
            p.write_bool(self.b);
        }
    }
}
