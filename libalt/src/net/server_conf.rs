use super::netable::Netable;
use packet::*;
use std::default::Default;

#[derive(Debug,Clone,Copy)]
pub enum GravMode{ Normal=0, One, Two, Three }
impl Default for GravMode{
    fn default() -> GravMode{ GravMode::Three }
}
#[derive(Debug,Clone,Copy)]
pub enum WeaponMode{ Normal=0, One, Two, Three }
impl Default for WeaponMode{
    fn default() -> WeaponMode{ WeaponMode::Two }
}

#[derive(Default,Debug,Clone)]
pub struct ServerConf{
    pub map_source:     String,
    pub test_em:        f32,
    pub health_mod:     f32,
    pub test_ds:        bool,
    pub scale:          u32,
    pub plane_scale:    u32,
    pub grav:           GravMode,
    pub weapons:        WeaponMode,
    pub unknown1:       u32,
    pub unknown2:       u32,
    // TODO: map & command lists go here
    // TODO: game mode configs
    pub prevent_switch: bool,
    pub no_bal_msg:     bool,
    pub unknown3:       bool,
    pub max_players:    u16,
    pub max_ping:       u32,
    pub unknown4:       bool,

    pub counter:        u32,
    pub data:           Option<(bool,Vec<u8>)>,
}

impl ServerConf{
    pub fn new() -> ServerConf{
        let mut ret = ServerConf::default();
        ret.test_em = 1f32; ret.health_mod = 1f32;
        ret.scale = 100u32; ret.plane_scale = 100u32;
        ret
    }
}

impl Netable for ServerConf{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let compressed = try!(p.read_bool());
        let mut data = Vec::new();
        let len = try!(p.read(12));
        for _ in 0..len{ data.push(try!(p.read_u8())) }
        let mut conf = ServerConf::new();
        conf.data = Some((compressed,data));
        Ok(conf)
    }
    fn write(&self, pkt: &mut BitStreamW){

        if let Some((ref compressed, ref data)) = self.data{
            pkt.write_bool(*compressed);
            pkt.write(12,data.len() as u64);
            pkt.write_bytes(data);
            return;
        }

        let mut p = BitStreamW::new();
        let mut pb = BitStreamW::new_le();

        p.context  = pkt.context;
        pb.context = pkt.context;
        p.shared   = pkt.shared.clone();
        pb.shared  = pkt.shared.clone();

        pb.write_str(&self.map_source[..]);
        pb.write_f32(self.test_em);
        pb.write_f32(self.health_mod);
        pb.write_bool(self.test_ds);
        pb.write_buf();
        p.write_bytes(&pb.data());

        p.write_range(40,300,self.scale as i32);
        p.write_range(40,300,self.plane_scale as i32);

        p.write_range(0,3,self.grav as i32);
        p.write_range(0,3,self.weapons as i32);
        p.write_range(0,1000,self.unknown1 as i32);
        p.write_range(0,1000,self.unknown2 as i32);

        p.write_u32(0); //lists
        p.write_u32(0);
        p.write_u32(0);

        p.write_str_l("<ffa/>");
        p.write_str_l("<tbd/>");
        p.write_str_l("<obj/>");
        p.write_str_l("<ball/>");
        p.write_str_l("<tdm/>");
        //p.write_str_l("<ffa scoreLimit='0' />");
        //p.write_str_l("<FreeForAllGameMode scoreLimit='0' />");
        //p.write_str_l("<BaseDestroyGameMode/>");
        //p.write_str_l("<ObjectiveGameModeConfig gamesPerRound='9' \
        //                gamesPerSwitchSides='2' gameWinMargin='1' \
        //                betweenGameTimeSeconds='6' />");
        //p.write_str_l("<PlaneBallGameModeConfig goalsPerRound='6' />");
        //p.write_str_l("<TeamDeathmatchGameMode scoreLimit='0'/>");

        p.write_bool(self.prevent_switch);
        p.write_bool(self.no_bal_msg);
        p.write_bool(self.unknown3);
        p.write_range(0,63,self.max_players as i32); //?
        p.write_range(0,1000,self.max_ping as i32); //?
        p.write_bool(self.unknown4);

        p.write_buf();

        pkt.write_bool(false); //compressed
        pkt.write(12,p.len() as u64);
        pkt.write_bytes(&p.data());
    }
}
