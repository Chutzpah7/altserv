use super::netable::Netable;
use packet::*;
use super::client_level::ClientLevel;
use super::uuid::Uuid;
use std::net::SocketAddr;

#[derive(Debug,Clone)]
pub struct ClientID{
    pub nick:  String,
    pub addr:  Option<SocketAddr>,
    pub vapor: Uuid,
    pub level: ClientLevel,
    pub login: bool,
    pub uuid:  Option<Uuid>,  //??
    pub unk1:  i64,
    pub unk2:  Vec<u16>,
    pub unk3:  Vec<u8>,
}

impl Netable for ClientID{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let level = try!(ClientLevel::read(p));
        let uuid = try!(Option::<Uuid>::read(p));

        let unk1 = try!(p.read_i64());
        let mut unk2 = Vec::new();

        unk2.push(try!(p.read_u16()));
        unk2.push(try!(p.read_u16()));
        unk2.push(try!(p.read_u16()));
        unk2.push(try!(p.read_u16()));
        unk2.push(try!(p.read_u16()));
        unk2.push(try!(p.read_u16()));
        unk2.push(try!(p.read_u16()));

        let len = try!(p.read(3));
        let mut unk3 = Vec::new();
        for _ in 0..len{ unk3.push(try!(p.read_u8())); }

        let addr = try!(Option::<SocketAddr>::read(p));
        let logged_in = try!(p.read_bool());
        let nick = try!(p.read_str());
        let vapor = try!(Uuid::read(p));

        try!(p.ignore(64)); // Skins maybe?..

        Ok(ClientID{
            nick:  nick,
            vapor: vapor,
            level: level,
            addr:  addr,
            login: logged_in,
            uuid:  uuid,
            unk1:  unk1,
            unk2:  unk2,
            unk3:  unk3,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        self.level.write(p);
        self.uuid.write(p);

        p.write_i64(self.unk1);
        for x in &self.unk2 { p.write_u16(*x); }
        p.write(3, self.unk3.len() as u64);
        for x in &self.unk3 { p.write_u8(*x); }

        self.addr.write(p);
        p.write_bool(self.login);
        p.write_str(&self.nick);
        self.vapor.write(p);
        p.write_u64(0);
    }
}
