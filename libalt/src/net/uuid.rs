use super::netable::Netable;
use packet::*;
use std::fmt;

#[derive(Clone,PartialEq)]
pub struct Uuid{
    a: u64,
    b: u64,
}

impl Uuid{
    pub fn new(a: u64, b: u64) -> Uuid{ Uuid{a: a, b: b} }
}

impl fmt::Debug for Uuid{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "uuid {{ a: {:x}, b: {:x} }}", self.a, self.b)
    }
}

impl Netable for Uuid{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(Uuid{a: try!(p.read_u64()), b: try!(p.read_u64())})
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_u64(self.a);
        p.write_u64(self.b);
    }
}
