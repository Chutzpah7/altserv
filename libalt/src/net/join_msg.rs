use packet::*;
use message::MsgData;
use state::*;
use net::netable::Netable;
use net::client_id::ClientID;
use net::version::Version;
use net::resource::Resource;
use net::server_conf::ServerConf;

#[derive(Debug,Clone)]
pub enum JoinMsg{
    JoinReq(ReqData),
    JoinResp(RespData),
    LoadReq(LoadReqData),
    LoadResp(LoadRespData),
    Disconnect,
    KeepAlive,
}

impl MsgData for JoinMsg {
    fn header(&self) -> u8 { 70 }
    fn write(&self, p: &mut BitStreamW){
        match *self{
            JoinMsg::JoinReq(ref x) => x.write(p),
            JoinMsg::JoinResp(ref x) => x.write(p),
            JoinMsg::LoadReq(ref x) => x.write(p),
            JoinMsg::LoadResp(ref x) => x.write(p),
            _ => (),
        }
    }
}
impl JoinMsg{
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        let t = try!(p.read(2));
        Ok(match p.context{
            Context::Server =>{ match t{
                0 => JoinMsg::JoinReq(try!(ReqData::read(p))),
                1 => JoinMsg::LoadReq(try!(LoadReqData::read(p))),
                2 => JoinMsg::Disconnect,
                3 => JoinMsg::KeepAlive,
                _ => unreachable!(),
            }},
            Context::Client =>{ match t{
                0 => JoinMsg::JoinResp(try!(RespData::read(p))),
                1 => JoinMsg::LoadResp(try!(LoadRespData::read(p))),
                2 => JoinMsg::Disconnect,
                3 => JoinMsg::KeepAlive,
                _ => unreachable!(),
            }}
        })
    }
}

#[derive(Debug,Clone)]
pub struct ReqData{
    pub version: Version,
    pub pass:    String,
    pub id:      ClientID,
}

impl Netable for ReqData{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(ReqData{
            version: try!(Version::read(p)),
            pass:    try!(p.read_str()),
            id:      try!(ClientID::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(2,0);
        self.version.write(p);
        p.write_str(&self.pass);
        self.id.write(p);
    }
}

#[derive(Debug,Clone)]
pub struct RespData{
    pub allow:  bool,
    pub reason: Option<String>,
}

impl Netable for RespData{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let reason = try!(Option::<String>::read(p));
        Ok(RespData{
            allow: reason.is_none(),
            reason: reason,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(2,0);
        p.write_bool(self.allow);
        if let Some(ref r) = self.reason{
            p.write_str(&r[..]);
        }
    }
}

#[derive(Debug,Clone)]
pub struct LoadReqData{
    pub resource: Resource,
}

impl Netable for LoadReqData{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(LoadReqData{
            resource: try!(Resource::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(2,1);
        self.resource.write(p);
    }
}

#[derive(Debug,Clone)]
pub struct LoadRespData{
    pub counter: u8,
    pub resource: Resource,
    pub conf: ServerConf,
}

impl ChangeState for LoadRespData{
    fn change_state(&self, shared: SharedState){
        let mut lock = shared.write();
        lock.set_map(&self.resource.path);
    }
}

impl Netable for LoadRespData{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(LoadRespData{
            counter: try!(p.read(2)) as u8,
            resource: try!(Resource::read(p)),
            conf: try!(ServerConf::read(p)),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(2,1);
        p.write(2,self.counter as u64);
        self.resource.write(p);
        self.conf.write(p);
    }
}
