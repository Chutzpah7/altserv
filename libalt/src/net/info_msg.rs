use packet::*;
use super::netable::Netable;
use ::message::MsgData;
use super::server_info::ServerInfo;

#[derive(Debug,Clone)]
pub enum InfoMsg{
    Request,
    Response(ServerInfo),
    Unknown
}
use self::InfoMsg::*;

impl MsgData for InfoMsg {
    fn header(&self) -> u8 { 7 }
    fn write(&self, p: &mut BitStreamW){
        match *self {
            Response(ref x) => {
                p.write(1,0);
                x.write(p);
            },
            _ => {},
        }
    }
}
impl InfoMsg{
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        if p.len() < 10 {
            Ok(Request)
        } else {
            warn!("got server info response, ignoring.");
            Ok(Unknown)
        }
    }
}

