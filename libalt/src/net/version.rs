use super::netable::Netable;
use packet::*;

pub const VERSION: Version = Version{ major: 0, minor: 0, patch: 374 };

#[derive(Debug,Clone)]
pub struct Version{
    pub major: i32,
    pub minor: i32,
    pub patch: i32,
}

impl Netable for Version{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let s = try!(p.read_str());
        let res: Result<Vec<i32>,_> = s.split(".")
            .map(|x| x.parse::<i32>()).collect();
        if let Ok(parts) = res{
            Ok(Version{
                major:parts[0],
                minor:parts[1],
                patch:parts[2]
            })
        }else{
            warn!("failed to parse version string, using 0.0.374");
            Ok(Version::new(0,0,374))
        }
    }
    fn write(&self, p: &mut BitStreamW){
        let s = format!("{}.{}.{}",self.major,self.minor,self.patch);
        p.write_str(&s[..]);
    }
}

impl Version{
    pub fn new(a: i32, b: i32, c: i32) -> Version{
        Version{
            major: a, minor: b, patch: c,
        }
    }
}
