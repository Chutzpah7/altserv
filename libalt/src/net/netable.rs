use packet::*;

/// A trait allowing structs to be read/read from/into packet data.
pub trait Netable : Sized{
    fn read(p: &mut BitStreamR) -> Res<Self>;
    fn write(&self, p: &mut BitStreamW);
}

impl<T: Netable> Netable for Option<T>{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let b = !try!(p.read_bool()); // yes, this is supposed to be inverted.
        Ok(if b { Some(try!(T::read(p))) } else { None })
    }
    fn write(&self, p: &mut BitStreamW){
        match *self{
            None => p.write_bool(true),
            Some(ref x) => {
                p.write_bool(false);
                x.write(p);
            },
        }
    }
}

impl Netable for String{
    fn read(p: &mut BitStreamR) -> Res<Self>{ Ok(try!(p.read_str())) }
    fn write(&self, p: &mut BitStreamW){ p.write_str(&self[..]); }
}

impl Netable for i32{
    fn read(p: &mut BitStreamR) -> Res<Self>{ Ok(try!(p.read_i32())) }
    fn write(&self, p: &mut BitStreamW){ p.write_i32(*self); }
}

impl<T: Netable> Netable for Vec<T>{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        let mut vec = Vec::new();
        for i in 0..try!(p.read_i32()){
            info!("reading item {}",i);
            vec.push(try!(T::read(p)));
        }
        Ok(vec)
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_i32(self.len() as i32);
        for i in self{
            i.write(p);
        }
    }
}
