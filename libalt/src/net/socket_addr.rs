use super::netable::Netable;
use packet::*;
use std::net::{SocketAddrV4,SocketAddrV6,SocketAddr};
use std::net::{Ipv4Addr,Ipv6Addr};
use std::mem::transmute;

impl Netable for SocketAddr{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        if try!(p.read_bool()) {
            let ip = Ipv4Addr::new(try!(p.read_u8()) ,try!(p.read_u8()),
                                   try!(p.read_u8()) ,try!(p.read_u8()));
            let port = try!(p.read_u32()) as u16;
            Ok(SocketAddr::V4(SocketAddrV4::new(ip,port)))
        } else {
            let ip = Ipv6Addr::new(try!(p.read_u16()),try!(p.read_u16()),try!(p.read_u16()),
                                   try!(p.read_u16()),try!(p.read_u16()),try!(p.read_u16()),
                                   try!(p.read_u16()),try!(p.read_u16()));
            let port = try!(p.read_u32()) as u16;
            Ok(SocketAddr::V6(SocketAddrV6::new(ip,port,0,0)))
        }
    }
    fn write(&self, p: &mut BitStreamW){
        let bytes: Vec<u8>;
        match *self{
            SocketAddr::V4(ref a) => {
                p.write_bool(true);
                bytes = a.ip().octets().to_vec();
            },
            SocketAddr::V6(ref a) => {
                p.write_bool(false);
                bytes = unsafe{
                    transmute::<[u16;8],[u8;16]>(a.ip().segments()).to_vec()
                };
            },
        }
        for b in bytes{ p.write_u8(b); }
        p.write_u32(self.port() as u32);
    }
}
