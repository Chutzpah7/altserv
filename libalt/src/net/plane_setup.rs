use super::netable::Netable;
use packet::*;

#[derive(Debug,Clone,Default)]
pub struct PlaneSetup{
    plane: PlaneType,
    random: RandomType,
    red_perk: i32,
    green_perk: i32,
    blue_perk: i32,
    skin: u64,
}

#[derive(Debug,Clone,Copy, PartialEq, Eq)]
pub enum PlaneType{ Biplane = 0, Bomber, Explodet, Loopy, Miranda, Unknown }

#[derive(Debug,Clone,Copy, PartialEq, Eq)]
pub enum RandomType{ None = 0, Config, Full }

impl Netable for PlaneSetup{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        if try!(p.read_bool()){
            use self::RandomType::*;
            let random = if try!(p.read_bool()) { Full } else { Config };
            Ok(PlaneSetup{
                plane:      PlaneType::Unknown,
                random:     random,
                red_perk:   0,
                green_perk: 0,
                blue_perk:  0,
                skin:       0
            })
        } else {
            Ok(PlaneSetup{
                plane:      try!(PlaneType::read(p)),
                random:     RandomType::None,
                red_perk:   try!(p.read_range(-4,11)),
                green_perk: try!(p.read_range(-4,11)),
                blue_perk:  try!(p.read_range(-4,11)),
                skin:       try!(p.read(8)),
            })
        }
    }
    fn write(&self, p: &mut BitStreamW){
        if self.random == RandomType::None{
            p.write_bool(false);
            self.plane.write(p);
            p.write_range(-4,11,self.red_perk);
            p.write_range(-4,11,self.green_perk);
            p.write_range(-4,11,self.blue_perk);
            p.write(8,self.skin);
        }
        else{ unimplemented!(); }
    }
}

impl Netable for PlaneType{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        use self::PlaneType::*;
        Ok(match try!(p.read_range(0,7)){
            0 => Biplane, 1 => Bomber, 2 => Explodet, 3 => Loopy,
            4 => Miranda, _ => Unknown,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_range(0,7,*self as i32);
    }
}

impl Netable for RandomType{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(match try!(p.read_range(0,3)){
            1 => RandomType::Config, 2 => RandomType::Full,
            _ => RandomType::None
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_range(0,3,*self as i32);
    }
}

impl Default for PlaneType{
    fn default() -> Self{ PlaneType::Loopy }
}
impl Default for RandomType{
    fn default() -> Self{ RandomType::None }
}
