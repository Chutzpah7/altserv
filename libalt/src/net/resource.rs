use super::netable::Netable;
use packet::*;

#[derive(Debug,Clone,Default)]
pub struct Resource{
    pub path:  String,
    pub size:  u32,
    pub crc32: u64
}

impl Netable for Resource{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(Resource{
            path:  try!(p.read_str()),
            size:  try!(p.read_u32()),
            crc32: try!(p.read_u64()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write_str(&self.path[..]);
        p.write_u32(self.size);
        p.write_u64(self.crc32);
    }
}
