use super::netable::Netable;
use packet::*;

#[derive(Debug,Clone,Copy,PartialEq)]
pub struct PlayerNo{ pub value: i32 }

impl Netable for PlayerNo{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(PlayerNo{ value: try!(p.read(8)) as i32 - 2})
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(8,(self.value + 2) as u64);
    }
}
