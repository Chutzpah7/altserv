use packet::*;
use super::netable::Netable;
use ::message::MsgData;
use std::net::SocketAddr;

#[derive(Debug,Clone)]
pub enum LanMsg{
    Request,
    Response(Option<SocketAddr>),
}

impl MsgData for LanMsg {
    fn header(&self) -> u8 { 74 }
    fn write(&self, p: &mut BitStreamW){
        match *self{
            LanMsg::Request => {},
            LanMsg::Response(ref x) => x.write(p),
        }
    }
}
impl LanMsg{
    pub fn read(p: &mut BitStreamR) -> Res<Self>{
        if p.len() == 2 {
            Ok(LanMsg::Request)
        } else {
            Ok(LanMsg::Response(try!(Option::read(p))))
        }
    }
}

