/// This module contains many of the small components which make up a message

pub mod netable;

pub mod join_msg;
pub mod game_msg;
pub mod ping_msg;
pub mod lan_msg;
pub mod info_msg;
pub mod dl_msg;

pub mod socket_addr;

pub mod net_id;

pub mod client_level;
pub mod client_id;
pub mod version;
pub mod uuid;
pub mod resource;
pub mod server_conf;
pub mod server_info;
pub mod playerno;

pub mod plane_setup;
pub mod team;
