use super::netable::Netable;
use packet::*;

#[derive(Debug,Clone)]
pub struct ClientLevel{
    pub ace:    u32,
    pub level:  u32,
    pub kills:  u32,
    pub deaths: u32,
}

impl Netable for ClientLevel{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        Ok(ClientLevel{
            ace:    try!(p.read(5)) as u32,
            level:  try!(p.read(7)) as u32,
            kills:  try!(p.read_u32()),
            deaths: try!(p.read_u32()),
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(5,self.ace as u64);
        p.write(7,self.level as u64);
        p.write_u32(self.kills);
        p.write_u32(self.deaths);
    }
}
