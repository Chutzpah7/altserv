use super::netable::Netable;
use packet::*;

#[derive(Debug,Clone,Copy)]
pub enum Team{
    A = 0, B, Spec, Red, Blue, Green, Yellow, Orange,
    Purple, Azure, Pink, Brown
}

impl Netable for Team{
    fn read(p: &mut BitStreamR) -> Res<Self>{
        use self::Team::*;
        Ok(match try!(p.read(8)){
            0 => A, 1 => B, 2 => Spec, 3 => Red, 4 => Blue, 5 => Green,
            6 => Yellow, 7 => Orange, 8 => Purple, 9 => Azure, 10 => Pink,
            11 => Brown, _ => Spec,
        })
    }
    fn write(&self, p: &mut BitStreamW){
        p.write(8, *self as u64);
    }
}
