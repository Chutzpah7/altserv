use std::ops::{Add,Sub};

#[cfg(feature = "serialize")]
extern crate rustc_serialize;

#[cfg(feature = "serialize")]
#[derive(Debug,Clone,Copy,RustcDecodable,RustcEncodable)]
pub struct Vec2<T>{ pub x: T, pub y: T }

#[cfg(not(feature = "serialize"))]
#[derive(Debug,Clone,Copy)]
pub struct Vec2<T>{ pub x: T, pub y: T }

impl<T> Vec2<T>{
    pub fn new(x: T, y: T) -> Vec2<T>{
        Vec2{ x: x, y: y }
    }
}

pub type Vec2f = Vec2<f32>;
pub type Vec2d = Vec2<f64>;

impl<T> Add<Vec2<T>> for Vec2<T> where T: Copy + Add<T, Output=T>{
    type Output = Vec2<T>;
    fn add(self, r: Vec2<T>) -> Vec2<T> {
        Vec2::<T>::new(self.x + r.x, self.y + r.y)
    }
}
impl<T> Sub<Vec2<T>> for Vec2<T> where T: Copy + Sub<T, Output=T>{
    type Output = Vec2<T>;
    fn sub(self, r: Vec2<T>) -> Vec2<T> {
        Vec2::<T>::new(self.x - r.x, self.y - r.y)
    }
}
